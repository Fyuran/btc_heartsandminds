// ACE Advanced Ballistics
force ace_advanced_ballistics_ammoTemperatureEnabled = false;
force ace_advanced_ballistics_barrelLengthInfluenceEnabled = false;
force ace_advanced_ballistics_bulletTraceEnabled = false;
force ace_advanced_ballistics_enabled = false;
force ace_advanced_ballistics_muzzleVelocityVariationEnabled = false;
force ace_advanced_ballistics_simulationInterval = 0.05;

// ACE Advanced Fatigue
force ace_advanced_fatigue_enabled = true;
force ace_advanced_fatigue_enableStaminaBar = true;
force ace_advanced_fatigue_loadFactor = 1;
force ace_advanced_fatigue_performanceFactor = 2;
force ace_advanced_fatigue_recoveryFactor = 2;
force ace_advanced_fatigue_swayFactor = 1;
force ace_advanced_fatigue_terrainGradientFactor = 0.8;

// ACE Advanced Throwing
force ace_advanced_throwing_enabled = true;
force ace_advanced_throwing_enablePickUp = true;
force ace_advanced_throwing_enablePickUpAttached = true;
force ace_advanced_throwing_showMouseControls = true;
force ace_advanced_throwing_showThrowArc = true;

// ACE Arsenal
force ace_arsenal_enableIdentityTabs = false;
force ace_arsenal_enableModIcons = true;
force ace_arsenal_EnableRPTLog = false;

// ACE Captives
force ace_captives_allowHandcuffOwnSide = true;
force ace_captives_allowSurrender = true;
force ace_captives_requireSurrender = 1;
force ace_captives_requireSurrenderAi = false;

// ACE Common
force ace_common_allowFadeMusic = true;
force ace_common_checkPBOsAction = 0;
force ace_noradio_enabled = true;
force ace_parachute_hideAltimeter = true;

// ACE Cook off
force ace_cookoff_ammoCookoffDuration = 1;
force ace_cookoff_enable = false;
force ace_cookoff_enableAmmobox = false;
force ace_cookoff_enableAmmoCookoff = false;
force ace_cookoff_probabilityCoef = 1;

// ACE Explosives
force ace_explosives_explodeOnDefuse = false;
force ace_explosives_punishNonSpecialists = false;
force ace_explosives_requireSpecialist = true;

// ACE Fragmentation Simulation
force ace_frag_enabled = true;
force ace_frag_maxTrack = 10;
force ace_frag_maxTrackPerFrame = 10;
force ace_frag_reflectionsEnabled = true;
force ace_frag_spallEnabled = true;

// ACE Hearing
force ace_hearing_autoAddEarplugsToUnits = true;
force ace_hearing_disableEarRinging = false;
force ace_hearing_earplugsVolume = 0.5;
force ace_hearing_enableCombatDeafness = true;
force ace_hearing_enabledForZeusUnits = false;
force ace_hearing_unconsciousnessVolume = 0.4;

// ACE Interaction
force ace_interaction_disableNegativeRating = true;
force ace_interaction_enableMagazinePassing = true;

// ACE Interaction Menu
ace_interact_menu_useListMenu = true;

// ACE Logistics
force ace_cargo_enable = true;
force ace_cargo_paradropTimeCoefficent = 2.5;
force ace_rearm_level = 0;
force ace_rearm_supply = 0;
force ace_refuel_hoseLength = 12;
force ace_refuel_rate = 10;
force ace_repair_addSpareParts = true;
force ace_repair_autoShutOffEngineWhenStartingRepair = true;
force ace_repair_consumeItem_toolKit = 0;
force ace_repair_displayTextOnRepair = true;
force ace_repair_engineerSetting_fullRepair = 1;
force ace_repair_engineerSetting_repair = 1;
force ace_repair_engineerSetting_wheel = 0;
force ace_repair_fullRepairLocation = 3;
force ace_repair_repairDamageThreshold = 0.6;
force ace_repair_repairDamageThreshold_engineer = 0.4;
force ace_repair_wheelRepairRequiredItems = 0;

// ACE Magazine Repack
force ace_magazinerepack_timePerAmmo = 1.5;
force ace_magazinerepack_timePerBeltLink = 8;
force ace_magazinerepack_timePerMagazine = 2;

// ACE Map
force ace_map_BFT_Enabled = false;
force ace_map_BFT_HideAiGroups = false;
force ace_map_BFT_Interval = 1;
force ace_map_BFT_ShowPlayerNames = false;
force ace_map_DefaultChannel = 0;
force ace_map_mapGlow = true;
force ace_map_mapIllumination = true;
force ace_map_mapLimitZoom = false;
force ace_map_mapShake = true;
force ace_map_mapShowCursorCoordinates = false;
force ace_markers_moveRestriction = 0;

// ACE Medical
force ace_medical_ai_enabledFor = 2;
force ace_medical_AIDamageThreshold = 0.5;
force ace_medical_allowLitterCreation = true;
force ace_medical_allowUnconsciousAnimationOnTreatment = true;
force ace_medical_amountOfReviveLives = -1;
force ace_medical_bleedingCoefficient = 1;
force ace_medical_consumeItem_PAK = 0;
force ace_medical_consumeItem_SurgicalKit = 0;
force ace_medical_delayUnconCaptive = 3;
force ace_medical_enableAdvancedWounds = false;
force ace_medical_enableFor = 0;
force ace_medical_enableOverdosing = true;
force ace_medical_enableRevive = 1;
force ace_medical_enableScreams = true;
force ace_medical_enableUnconsciousnessAI = 0;
force ace_medical_enableVehicleCrashes = true;
force ace_medical_healHitPointAfterAdvBandage = true;
force ace_medical_increaseTrainingInLocations = true;
force ace_medical_keepLocalSettingsSynced = true;
force ace_medical_level = 1;
force ace_medical_litterCleanUpDelay = 30;
force ace_medical_litterSimulationDetail = 3;
force ace_medical_maxReviveTime = 600;
force ace_medical_medicSetting = 1;
force ace_medical_medicSetting_basicEpi = 1;
force ace_medical_medicSetting_PAK = 1;
force ace_medical_medicSetting_SurgicalKit = 1;
force ace_medical_menu_allow = 1;
force ace_medical_menu_maxRange = 3;
force ace_medical_menu_openAfterTreatment = true;
force ace_medical_menu_useMenu = 2;
force ace_medical_menuTypeStyle = 1;
force ace_medical_moveUnitsFromGroupOnUnconscious = false;
force ace_medical_painCoefficient = 1;
force ace_medical_painEffectType = 0;
force ace_medical_painIsOnlySuppressed = true;
force ace_medical_playerDamageThreshold = 5;
force ace_medical_preventInstaDeath = true;
force ace_medical_remoteControlledAI = true;
force ace_medical_useCondition_PAK = 0;
force ace_medical_useCondition_SurgicalKit = 0;
force ace_medical_useLocation_basicEpi = 0;
force ace_medical_useLocation_PAK = 0;
force ace_medical_useLocation_SurgicalKit = 0;

// ACE Mk6 Mortar
force ace_mk6mortar_airResistanceEnabled = false;
force ace_mk6mortar_allowCompass = true;
force ace_mk6mortar_allowComputerRangefinder = true;
force ace_mk6mortar_useAmmoHandling = false;

// ACE Nightvision
force ace_nightvision_aimDownSightsBlur = 0.0861546;
force ace_nightvision_disableNVGsWithSights = false;
force ace_nightvision_effectScaling = 0.1;
force ace_nightvision_fogScaling = 0.264407;
force ace_nightvision_noiseScaling = 0.1;
force ace_nightvision_shutterEffects = true;

// ACE Overheating
force ace_overheating_displayTextOnJam = true;
force ace_overheating_enabled = true;
force ace_overheating_overheatingDispersion = true;
force ace_overheating_showParticleEffects = true;
force ace_overheating_showParticleEffectsForEveryone = true;
force ace_overheating_unJamFailChance = 0.1;
force ace_overheating_unJamOnreload = true;

// ACE Pointing
force ace_finger_enabled = true;
force ace_finger_indicatorForSelf = true;
force ace_finger_maxRange = 4;

// ACE Pylons
force ace_pylons_enabledForZeus = true;
force ace_pylons_enabledFromAmmoTrucks = true;
force ace_pylons_rearmNewPylons = true;
force ace_pylons_requireEngineer = false;
force ace_pylons_requireToolkit = false;
force ace_pylons_searchDistance = 15;
force ace_pylons_timePerPylon = 5;

// ACE Quick Mount
ace_quickmount_enabled = true;

// ACE Respawn
force ace_respawn_removeDeadBodiesDisconnected = true;
force ace_respawn_savePreDeathGear = true;

// ACE Scopes
force ace_scopes_correctZeroing = true;
force ace_scopes_deduceBarometricPressureFromTerrainAltitude = false;
force ace_scopes_defaultZeroRange = 100;
force ace_scopes_enabled = true;
force ace_scopes_forceUseOfAdjustmentTurrets = false;
force ace_scopes_overwriteZeroRange = true;
force ace_scopes_simplifiedZeroing = false;
force ace_scopes_zeroReferenceBarometricPressure = 1013.25;
force ace_scopes_zeroReferenceHumidity = 0;
force ace_scopes_zeroReferenceTemperature = 15;

// ACE Spectator
force ace_spectator_enableAI = false;
force ace_spectator_restrictModes = 0;
force ace_spectator_restrictVisions = 0;

// ACE Switch Units
force ace_switchunits_enableSafeZone = true;
force ace_switchunits_enableSwitchUnits = false;
force ace_switchunits_safeZoneRadius = 100;
force ace_switchunits_switchToCivilian = false;
force ace_switchunits_switchToEast = false;
force ace_switchunits_switchToIndependent = false;
force ace_switchunits_switchToWest = false;

// ACE Tagging
force ace_tagging_quickTag = 1;

// ACE Uncategorized
force ace_gforces_enabledFor = 2;
force ace_hitreactions_minDamageToTrigger = 0.1;
force ace_inventory_inventoryDisplaySize = 0;
force ace_laser_dispersionCount = 2;
force ace_microdagr_mapDataAvailable = 2;
force ace_optionsmenu_showNewsOnMainMenu = false;
force ace_overpressure_distanceCoefficient = 1;

// ACE Vehicle Lock
force ace_vehiclelock_defaultLockpickStrength = 10;
force ace_vehiclelock_lockVehicleInventory = false;
force ace_vehiclelock_vehicleStartingLockState = -1;

// ACE View Distance Limiter
force force ace_viewdistance_enabled = true;
force force ace_viewdistance_limitViewDistance = 12000;

// ACE Weapons
force ace_common_persistentLaserEnabled = false;
force ace_laserpointer_enabled = true;
force ace_reload_displayText = true;
force ace_weaponselect_displayText = true;

// ACE Weather
force force ace_weather_enabled = true;
force force ace_weather_updateInterval = 60;
force force ace_weather_windSimulation = true;

// ACE Wind Deflection
force force ace_winddeflection_enabled = true;
force force ace_winddeflection_simulationInterval = 0.05;
force force ace_winddeflection_vehicleEnabled = true;

// ACE Zeus
force ace_zeus_autoAddObjects = true;
force ace_zeus_radioOrdnance = false;
force ace_zeus_remoteWind = false;
force ace_zeus_revealMines = 0;
force ace_zeus_zeusAscension = false;
force ace_zeus_zeusBird = false;

// ACEX Fortify
force acex_fortify_settingHint = 2;

// ACEX Headless
force force acex_headless_delay = 120;
force force acex_headless_enabled = true;
force force acex_headless_log = false;

// ACEX Sitting
force force acex_sitting_enable = true;