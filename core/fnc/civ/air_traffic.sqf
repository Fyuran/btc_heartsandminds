
/* ----------------------------------------------------------------------------
Function: btc_fnc_civ_air_traffic

Description:
    Fill me when you edit me !

Parameters:
    _list - [Array]

Returns:

Examples:
    (begin example)
    (end)

Author:
    Fyuran

---------------------------------------------------------------------------- */
params[
	["_city",objNull,[objNull]],
	["_p_type_planes", btc_civ_type_planes, [[]]],
    ["_p_type_helicopters", btc_civ_type_helicopters, [[]]],
    ["_p_civ_air_traffic_max", btc_p_civ_air_traffic_max, [0]]
];

private _has_traffic = _city getVariable["air_group",grpNull];
if(!isNull _has_traffic)exitWith{diag_log format["air_traffic.sqf: attempted to spawn additional air_group,%1",_this]};
private _type = [selectRandom _p_type_planes,selectRandom _p_type_helicopters] select (random 1>0.15);

private _group = createGroup [civilian,true];
_group setVariable ["no_cache",true];
_group setVariable ["city",_city];
_group setBehaviour "CARELESS";
(units _group) apply {[_x] call btc_fnc_civ_unit_create};
private _flightheight = 300;

private _pos = _city getPos [(random[3000,4000,_flightheight])*_p_civ_air_traffic_max, random 360];

private _veh_array = [_pos,_pos getDir _city,_type,_group] call BIS_fnc_spawnVehicle; //Array - 0: created vehicle (Object), 1: all crew (Array of Objects), 2: vehicle's group (Group)
(_veh_array select 1) apply {_x disableAI "TARGET"; _x disableAI "AUTOTARGET"};
private _veh = _veh_array select 0;
leader _group setVariable ["acex_headless_blacklist", true];
private _vel = velocity _veh;
private _dir = getDir _veh;
private _speed = 100;
_veh setPosATL[(_pos select 0),(_pos select 1),_flightheight];
_veh setVelocity [
	(_vel select 0) + (sin _dir * _speed),
	(_vel select 1) + (cos _dir * _speed),
	(_vel select 2)
];

private _eh = _veh addEventHandler ["Killed", {
    private _group = group (_this select 0);
    private _city = _group getVariable ["city",selectRandom btc_city_all];
	_group call CBA_fnc_deleteEntity;
	(_this select 0)call CBA_fnc_deleteEntity;
    _city setVariable ["air_group", grpNull];
    [_city] call btc_fnc_civ_air_traffic;
}];

//Add WP
_pos = _city getPos [(random[3000,4000,_flightheight])*_p_civ_air_traffic_max,(objectParent leader _group) getDir _city];

private _wp = _group addWaypoint [_pos, 0];
_wp setWaypointPosition [[(_pos select 0),(_pos select 1),_flightheight], 0];

_wp setWaypointType "MOVE";
_wp setWaypointCompletionRadius 1000;
_wp setWaypointStatements ["true","
    private _group = (group this);
    private _city = _group getVariable ['city',selectRandom btc_city_all];
    (objectParent this) call CBA_fnc_deleteEntity;
    _group call CBA_fnc_deleteEntity;
    _city setVariable ['air_group', grpNull];
    [_city] call btc_fnc_civ_air_traffic;
"];

_veh flyInHeight _flightheight;
_city setVariable ["air_group", _group];

if (btc_debug) then {
    private _marker = createmarker [format ["btc_air_traffic_%1", _group] , _pos];
    _marker setmarkertype "mil_dot";
    _marker setMarkerText format ["A_P %1", _group];
    _marker setmarkerColor "ColorYellow";
    _marker setMarkerSize [0.5, 0.5];
    [format ["AIR TRAFFIC %1 WP POS: %2",_group ,_pos], __FILE__, [false]] call btc_fnc_debug_message;
};

