
params[
    ["_snowSpeed", 5, [0]]
];

if(!btc_effects_enabled) exitWith {};

_snow_sound_handle =
[
    {
        playSound "btc_sound_ambience_snow";
        if(!btc_effects_enabled) then {[_handle] call CBA_fnc_removePerFrameHandler};
    },
    58,
    []
] call CBA_fnc_addPerFrameHandler;

private _snow_particles_handle = [{
    _inside = [player] call btc_fnc_effects_isInside;
    if (!_inside) then{
        _pos = getPosATL (vehicle player);
        _snow = [((_pos select 0) + (_args - (random (2*_args))) + ((velocity vehicle player select 0)*1)),((_pos select 1) + (_args - (random (2*_args))) + ((velocity vehicle player select 0)*1)),((_pos select 2) + 2)];
        drop ["\ca\data\cl_water", "", "Billboard", 1, 7, _snow, [0,0,-1], 1, 0.0000001, 0.000, 0.7, [0.07], [[1,1,1,0], [1,1,1,1], [1,1,1,1], [1,1,1,1]], [0,0], 0.2, 1.2, "", "", ""];
        _snow = [((_pos select 0) + (_args - (random (2*_args))) + ((velocity vehicle player select 0)*1)),((_pos select 1) + (_args - (random (2*_args))) + ((velocity vehicle player select 0)*1)),((_pos select 2) + 4)];
        drop ["\ca\data\cl_water", "", "Billboard", 1, 7, _snow, [0,0,-1], 1, 0.0000001, 0.000, 0.7, [0.07], [[1,1,1,0], [1,1,1,1], [1,1,1,1], [1,1,1,1]], [0,0], 0.2, 1.2, "", "", ""];
        _snow = [((_pos select 0) + (_args - (random (2*_args))) + ((velocity vehicle player select 0)*1)),((_pos select 1) + (_args - (random (2*_args))) + ((velocity vehicle player select 0)*1)),((_pos select 2) + 6)];
        drop ["\ca\data\cl_water", "", "Billboard", 1, 7, _snow, [0,0,-1], 1, 0.0000001, 0.000, 0.7, [0.07], [[1,1,1,0], [1,1,1,1], [1,1,1,1], [1,1,1,1]], [0,0], 0.2, 1.2, "", "", ""];
        _snow = [((_pos select 0) + (_args - (random (2*_args))) + ((velocity vehicle player select 0)*1)),((_pos select 1) + (_args - (random (2*_args))) + ((velocity vehicle player select 0)*1)),((_pos select 2) + 8)];
        drop ["\ca\data\cl_water", "", "Billboard", 1, 7, _snow, [0,0,-1], 1, 0.0000001, 0.000, 0.7, [0.07], [[1,1,1,0], [1,1,1,1], [1,1,1,1], [1,1,1,1]], [0,0], 0.2, 1.2, "", "", ""];
        _snow = [((_pos select 0) + (_args - (random (2*_args))) + ((velocity vehicle player select 0)*1)),((_pos select 1) + (_args - (random (2*_args))) + ((velocity vehicle player select 0)*1)),((_pos select 2) + 10)];
        drop ["\ca\data\cl_water", "", "Billboard", 1, 7, _snow, [0,0,-1], 1, 0.0000001, 0.000, 0.7, [0.07], [[1,1,1,0], [1,1,1,1], [1,1,1,1], [1,1,1,1]], [0,0], 0.2, 1.2, "", "", ""];
        _snow = [((_pos select 0) + (_args - (random (2*_args))) + ((velocity vehicle player select 0)*1)),((_pos select 1) + (_args - (random (2*_args))) + ((velocity vehicle player select 0)*1)),((_pos select 2) + 12)];
        drop ["\ca\data\cl_water", "", "Billboard", 1, 7, _snow, [0,0,-1], 1, 0.0000001, 0.000, 0.7, [0.07], [[1,1,1,0], [1,1,1,1], [1,1,1,1], [1,1,1,1]], [0,0], 0.2, 1.2, "", "", ""];
    };
    if(!btc_effects_enabled) then {[_handle] call CBA_fnc_removePerFrameHandler};
}, 0.1, _snowSpeed] call CBA_fnc_addPerFrameHandler;

player setVariable["btc_effect_PFH_snow",[_snow_sound_handle,_snow_particles_handle]];
