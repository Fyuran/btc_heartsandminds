
params[
    ["_snowSpeed", 5, [0]]
];

if(!btc_effects_enabled) exitWith {};

enableEnvironment false;

private _snow_sound_handle =
[
    {
        playSound "btc_sound_fog";
        if(!btc_effects_enabled) then {[_handle] call CBA_fnc_removePerFrameHandler; enableEnvironment true};
    },
    18,
    []
] call CBA_fnc_addPerFrameHandler;

private _PP_colorC = ppEffectCreate ["ColorCorrections",1500];
_PP_colorC ppEffectEnable true;
_PP_colorC ppEffectAdjust [1,1,0,[0,0,0,0],[1,1,1,0.64],[0.33,0.33,0.33,0],[0,0,0,0,0,0,4]];
_PP_colorC ppEffectCommit 1;

private _PP_film = ppEffectCreate ["FilmGrain",2000];
_PP_film ppEffectEnable true;
_PP_film ppEffectAdjust [0.25,1,1,0.5,0.5,true];
_PP_film ppEffectCommit 1;

private _snow_particles_handle = [{
    _inside = [player] call btc_fnc_effects_isInside;
    if (!_inside) then{
        _pos = getPosATL (vehicle player);
        _snow = [((_pos select 0) + (_args - (random (2*_args))) + ((velocity vehicle player select 0)*1)),((_pos select 1) + (_args - (random (2*_args))) + ((velocity vehicle player select 0)*1)),((_pos select 2) + 2)];
        drop ["\ca\data\cl_water", "", "Billboard", 1, 7, _snow, [0,0,-1], 1, 0.0000001, 0.000, 0.7, [0.07], [[1,1,1,0], [1,1,1,1], [1,1,1,1], [1,1,1,1]], [0,0], 0.2, 1.2, "", "", ""];
        _snow = [((_pos select 0) + (_args - (random (2*_args))) + ((velocity vehicle player select 0)*1)),((_pos select 1) + (_args - (random (2*_args))) + ((velocity vehicle player select 0)*1)),((_pos select 2) + 4)];
        drop ["\ca\data\cl_water", "", "Billboard", 1, 7, _snow, [0,0,-1], 1, 0.0000001, 0.000, 0.7, [0.07], [[1,1,1,0], [1,1,1,1], [1,1,1,1], [1,1,1,1]], [0,0], 0.2, 1.2, "", "", ""];
        _snow = [((_pos select 0) + (_args - (random (2*_args))) + ((velocity vehicle player select 0)*1)),((_pos select 1) + (_args - (random (2*_args))) + ((velocity vehicle player select 0)*1)),((_pos select 2) + 6)];
        drop ["\ca\data\cl_water", "", "Billboard", 1, 7, _snow, [0,0,-1], 1, 0.0000001, 0.000, 0.7, [0.07], [[1,1,1,0], [1,1,1,1], [1,1,1,1], [1,1,1,1]], [0,0], 0.2, 1.2, "", "", ""];
        _snow = [((_pos select 0) + (_args - (random (2*_args))) + ((velocity vehicle player select 0)*1)),((_pos select 1) + (_args - (random (2*_args))) + ((velocity vehicle player select 0)*1)),((_pos select 2) + 8)];
        drop ["\ca\data\cl_water", "", "Billboard", 1, 7, _snow, [0,0,-1], 1, 0.0000001, 0.000, 0.7, [0.07], [[1,1,1,0], [1,1,1,1], [1,1,1,1], [1,1,1,1]], [0,0], 0.2, 1.2, "", "", ""];
        _snow = [((_pos select 0) + (_args - (random (2*_args))) + ((velocity vehicle player select 0)*1)),((_pos select 1) + (_args - (random (2*_args))) + ((velocity vehicle player select 0)*1)),((_pos select 2) + 10)];
        drop ["\ca\data\cl_water", "", "Billboard", 1, 7, _snow, [0,0,-1], 1, 0.0000001, 0.000, 0.7, [0.07], [[1,1,1,0], [1,1,1,1], [1,1,1,1], [1,1,1,1]], [0,0], 0.2, 1.2, "", "", ""];
        _snow = [((_pos select 0) + (_args - (random (2*_args))) + ((velocity vehicle player select 0)*1)),((_pos select 1) + (_args - (random (2*_args))) + ((velocity vehicle player select 0)*1)),((_pos select 2) + 12)];
        drop ["\ca\data\cl_water", "", "Billboard", 1, 7, _snow, [0,0,-1], 1, 0.0000001, 0.000, 0.7, [0.07], [[1,1,1,0], [1,1,1,1], [1,1,1,1], [1,1,1,1]], [0,0], 0.2, 1.2, "", "", ""];
    };
    if(!btc_effects_enabled) then {[_handle] call CBA_fnc_removePerFrameHandler};
}, 0.1, _snowSpeed] call CBA_fnc_addPerFrameHandler;
