
params[
    ["_center",objNull,[objNull]]
];

private _inside = false;
private _worldPos = getPosWorld _center;
private _skyPos = getPosWorld _center vectorAdd [0, 0, 50];
private _line = lineIntersectsSurfaces [_worldPos,_skyPos,_center,objNull,true,1,"GEOM","NONE"];

if (count _line > 0) then
{
    private _result = _line select 0;
    private _house = _result select 3;
    if (_house isKindOf "House") then { _inside = true };
};

_inside
