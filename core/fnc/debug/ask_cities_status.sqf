
/* ----------------------------------------------------------------------------
Function: btc_debug_ask_cities_status

Description:

Parameters:

Returns:

Examples:
    (begin example)
        [clientOwner] RemoteExeCall [btc_debug_ask_cities_status,2];
    (end)

Author:
    =BTC=Fyuran

---------------------------------------------------------------------------- */
params[
	["_clientid",clientOwner,[0]]
];
if(btc_debug) exitWith {};

private _markers = [];
btc_city_all apply {
	_id = _x getVariable["id",0];
	_pos = getPosASL _x;
	_size = (_x getVariable["RadiusX",0]) + ( _x getVariable["RadiusY",0]) + btc_city_radius;
    _color = ["ColorGreen","ColorRed"] select (_x getVariable["occupied",false]);
    _has_ho = _x getVariable["has_ho",false];
    if(_has_ho) then {_size = btc_hideouts_radius + btc_city_radius};
    _markers pushBack [_id,_pos, _size, _color, _has_ho];
};

[_markers,{
	private _markers = [];
	_this apply {
		_x params["_id","_pos", "_size", "_color", "_has_ho"];
		private _marker = createMarkerLocal [format ["loc_%1", _id], _pos];
	    _marker setMarkerShapeLocal "ELLIPSE";
	    _marker setMarkerBrushLocal "SolidBorder";
	    _marker setMarkerSizeLocal [_size, _size];
	    _marker setMarkerColorLocal ([_color,"ColorBLUFOR"] select _has_ho);
	    _marker setMarkerAlphaLocal 0.3;
	    _markers pushBackUnique _marker;
	};
	player setVariable["btc_debug_asked_markers", _markers];
}] remoteExecCall["call",_clientid];