
/* ----------------------------------------------------------------------------
Function: btc_fnc_debug_marker_show_fps_init

Description:
    Init for the fps markers

Parameters:

Returns:

Examples:
    (begin example)
    (end)

Author:
    =BTC=Fyuran

---------------------------------------------------------------------------- */
params
[
	["_strcorner","", [""]],
	["_distance",500,[0]]
];

private _Map_size = getNumber (configfile >> "CfgWorlds" >> worldName >> "mapSize");
private _corner = switch (_strcorner) do {
	default {[0,0]}; //south_west
	case "south_east" : {[_Map_size,0]};
	case "north_west" : {[0,_Map_size]};
	case "north_east" : {[_Map_size,_Map_size]};
};

["Server",_corner,_distance,0] call btc_fnc_debug_marker_show_fps;

[{!((entities "HeadlessClient_F") isEqualTo [])},{
	params["_corner","_distance"];
	{
		[format["HC%1",_foreachIndex +1],_corner,_distance,_foreachIndex + 1] remoteExecCall ["btc_fnc_debug_marker_show_fps",_x];
	}forEach entities "HeadlessClient_F";
},[_corner,_distance]] call CBA_fnc_waitUntilAndExecute;

true