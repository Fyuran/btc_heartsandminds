
/* ----------------------------------------------------------------------------
Function: btc_fnc_city_trigger_player_side

Description:
    Triggers city activation by invisible AI dummy beloging to btc_player_side

Parameters:
    _name - Name of the city. [String]


Returns:

Examples:
    (begin example)
    (end)

Author:
    Fyuran

---------------------------------------------------------------------------- */
params[
	["_name","",[""]]
];

private _index = btc_city_all findif {_x getVariable ["name",""] isEqualTo _name};
if(_index isEqualTo -1) exitWith {hint "Incorrect city name"};

private _city = btc_city_all select _index;
if(!isNil "btc_city_dummy") exitWith{btc_city_dummy setPosASL (GetPosASL _city)};
private _group = createGroup [btc_player_side, true];
btc_city_dummy = _group createUnit ["B_Soldier_VR_F", getPosASL _city, [], 0, "CAN_COLLIDE"];
btc_city_dummy enableSimulationGlobal false;
btc_city_dummy hideObjectGlobal true;

if (btc_debug_log) then {
    [format ["btc_city_dummy placed at city %1 - pos %2", _name, getPosASL _city], __FILE__, [false]] call btc_fnc_debug_message;
};

btc_city_dummy