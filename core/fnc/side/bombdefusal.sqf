/* ----------------------------------------------------------------------------
Function: btc_fnc_side_bombdefusal

Description:
    Fill me when you edit me !

Parameters:
    _data - [Number from 0 to 9,Color String either red,blue or green]

Returns:

Examples:
    (begin example)
        _result = [] call btc_fnc_side_bombdefusal;
    (end)

Author:
    Fyuran

---------------------------------------------------------------------------- */
params[
	["_attempts",ceil(random 3),[3]],
	["_timer",round(random [60, 85, 120]),[0]],
	["_data",[]],
	["_city","DUMMY",[""]]
];
private _digits = 4; //_digits as of 18/02/2019 is currently unused
_city = call {
	private _i = btc_city_all findIf {_x getVariable "name" isEqualTo _city};
	if(_i isEqualTo -1) exitWith {selectRandom (btc_city_all select { _x getVariable ["occupied", false] && {!( (_x getVariable ["type", ""]) in ["Hill","RockyArea"])} } ) };
	private _city = btc_city_all select _i;
	_city setVariable ["occupied",true];
	(_city getVariable ["marker", ""]) setMarkerColor "ColorRed";
	_city
};

if (_city isEqualTo []) exitWith {[] spawn btc_fnc_side_create;};

private _pos = [getPos _city, 100] call btc_fnc_randomize_pos;

btc_side_aborted = false;
btc_side_done = false;
btc_side_failed = false;
btc_side_assigned = true;
publicVariable "btc_side_assigned";
private _isMultiplayer = [0,-2] select isMultiplayer;

btc_side_jip_data = [18, getPos _city, _city getVariable "name"];
btc_side_jip_data remoteExec ["btc_fnc_task_create", 0];

_city setVariable ["spawn_more",true];

private _comp = [_pos,0,[
	["Land_Device_disassembled_F",[-1.10742,-1.46375,0],121.499,1,0,[],"","",true,false],
	["RoadBarrier_F",[0.943359,1.00195,0],209.807,1,0,[],"","",true,false],
	["TapeSign_F",[-1.67383,1.37866,0],178.603,1,0,[],"","",true,false],
	["TapeSign_F",[2.44922,-1.37891,0],239.241,1,0,[],"","",true,false],
	["RoadBarrier_F",[-4.05664,0.376953,0],120.35,1,0,[],"","",true,false],
	["TapeSign_F",[-3.93555,-2.2417,0],65,1,0,[],"","",true,false],
	["RoadBarrier_F",[-1.68164,-4.24805,0],32.2086,1,0,[],"","",true,false],
	["TapeSign_F",[1.32617,-5.12158,0],180.667,1,0,[],"","",true,false],
	["Land_PortableLight_double_F",[-5.30859,-1.64258,0],261.437,1,0,[],"","",true,false],
	["Danger",[0.578125,-5.62451,0],185.753,1,0,[],"","",true,false]
]] call BIS_fnc_ObjectsMapper;

private _bomb = _comp select 0;
_bomb allowDamage false;
private _marker = createMarker [format ["bomb_loc_%1", _bomb], getPos _city];
_marker setMarkerShape "ELLIPSE";
_marker setMarkerBrush "SolidBorder";
_marker setMarkerSize [100, 100];
_marker setMarkerAlpha 0.3;
_marker setMarkerColor "colorOrange";
if(btc_debug_log) then {
	private _marker = createMarker [format ["sm_18_%1", _pos], _pos];
	_marker setMarkerType "hd_flag";
	_marker setMarkerSize [0.6, 0.6];
};
//BOMB DEFUSAL DATA PROCESSING
if(_data isEqualTo []) then {
	for "_i" from 1 to _attempts do {
		private _code = "";
		private _color = [];
		for "_i" from 1 to _digits do {
			_code = _code + str(round(random 9));
			_color pushBack (selectRandom [[1,0,0,1],[0,1,0,1],[0,0,1,1]]);
		};
		_data pushBack [_code,_color];
	};
}else{

};
_bomb setVariable ["bombdefusal_data", [_data,[_attempts,_digits,_timer]],true];

[_bomb,{
	private _action = ["bombdefusal_decode", "Get Defuse Codes", "\a3\ui_f\data\IGUI\Cfg\HoldActions\holdAction_connect_ca.paa", {
		createDialog "bombdefusal_defuse_gui";
		player setVariable ["bombdefusal_bomb", _target];
		[_target] remoteExecCall ["btc_fnc_side_bombdefusal_timer",2];
		[] call btc_fnc_side_bombdefusal_gui_timer;
	}, {"ToolKit" in ((vestItems player) + (uniformItems player) + (backpackItems player))}] call ace_interact_menu_fnc_createAction;
	[_this, 0, ["ACE_MainActions"], _action] call ace_interact_menu_fnc_addActionToObject;

	_action = ["bombdefusal_defuse", "Defuse Device", "\a3\ui_f\data\IGUI\Cfg\HoldActions\holdAction_hack_ca.paa", {
		createDialog "bombdefusal_defuse_gui";
		player setVariable ["bombdefusal_bomb", _target];
		[_target] remoteExecCall ["btc_fnc_side_bombdefusal_timer",2];
		[] call btc_fnc_side_bombdefusal_gui_timer;
	}, {"ACE_DefusalKit" in ((vestItems player) + (uniformItems player) + (backpackItems player))}] call ace_interact_menu_fnc_createAction;
	[_this, 0, ["ACE_MainActions"], _action] call ace_interact_menu_fnc_addActionToObject;
}] remoteExecCall ["call", _isMultiplayer, _bomb];
//---------
waitUntil {
	sleep 5;
	(btc_side_aborted || btc_side_failed || btc_side_done)
};

private _timer_handle = _bomb getVariable ["bombdefusal_timer_handle",0];

if (btc_side_failed || btc_side_aborted) exitWith {
    18 remoteExec ["btc_fnc_task_fail", 0];
    [_bomb,0,["ACE_MainActions","bombdefusal_defuse"]] remoteExecCall ["ace_interact_menu_fnc_removeActionFromObject",_isMultiplayer,_bomb];
	[_bomb,0,["ACE_MainActions","bombdefusal_decode"]] remoteExecCall ["ace_interact_menu_fnc_removeActionFromObject",_isMultiplayer,_bomb];
	[1] remoteExecCall ["closeDialog",_isMultiplayer];
    [_timer_handle] call CBA_fnc_removePerFrameHandler;
    if(btc_side_failed) then {[_pos,300,5,true] call btc_fnc_side_bombdefusal_nuke_fail};
    [[_marker], _comp] call btc_fnc_delete;
    btc_side_done = nil;publicVariable "btc_side_done";
    btc_side_assigned = false;publicVariable "btc_side_assigned";
};

if (btc_debug_log) then {
	["bomb_defusal is done", __FILE__, [false]] call btc_fnc_debug_message;
};

btc_side_done = nil;publicVariable "btc_side_done";
btc_side_assigned = false;publicVariable "btc_side_assigned";
[_timer_handle] call CBA_fnc_removePerFrameHandler;

[[_marker], _comp] call btc_fnc_delete;
80 call btc_fnc_rep_change;
18 remoteExec ["btc_fnc_task_set_done", 0];
