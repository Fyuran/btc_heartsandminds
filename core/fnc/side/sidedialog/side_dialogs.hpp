#include "defines.hpp"
//Guarded-Macros autism;
#ifndef CUSTOM
	#define CUSTOM ctrlText 1599
#endif
#ifndef TASK
	#define TASK(VAR1) [VAR1, CUSTOM] RemoteExec ["btc_fnc_side_dialog_create", 2]
#endif

class SideMission_Dialog {
	idd = 19337;
	MovingEnabled = true;

	class controls {
		class SideBackground: btc_side_RscPicture
		{
			idc = 1201;

			text = "core\BTCScripts\Client\sidedialog\ui\btc_dialog_menu_textureA.paa";
			x = 0.267969 * safezoneW + safezoneX;
			y = 0.115 * safezoneH + safezoneY;
			w = 0.211406 * safezoneW;
			h = 0.902 * safezoneH;
		};
		class SideCustom: RscEdit
		{
			idc = 1599;
			x = 0.340156 * safezoneW + safezoneX;
			y = 0.764 * safezoneH + safezoneY;
			w = 0.061875 * safezoneW;
			h = 0.022 * safezoneH;
		};
		class SideMButton1: btc_side_RscButton
		{
			idc = 1600;
			text = "1 - SM - Supply";
			x = 0.298906 * safezoneW + safezoneX;
			y = 0.258 * safezoneH + safezoneY;
			w = 0.150000 * safezoneW;
			h = 0.022 * safezoneH;
			action = TASK(0);
		};
		class SideMButton2: btc_side_RscButton
		{
			idc = 1601;
			text = "2 - SM - Mine sweeper";
			x = 0.298906 * safezoneW + safezoneX;
			y = 0.291 * safezoneH + safezoneY;
			w = 0.150000 * safezoneW;
			h = 0.022 * safezoneH;
			action = TASK(1);
		};
		class SideMButton3: btc_side_RscButton
		{
			idc = 1602;
			text = "3 - SM - Vehicle";
			x = 0.298906 * safezoneW + safezoneX;
			y = 0.324 * safezoneH + safezoneY;
			w = 0.150000 * safezoneW;
			h = 0.022 * safezoneH;
			action = TASK(2);
		};
		class SideMButton4: btc_side_RscButton
		{
			idc = 1603;
			text = "4 - SM - Get City";
			x = 0.298906 * safezoneW + safezoneX;
			y = 0.357 * safezoneH + safezoneY;
			w = 0.150000 * safezoneW;
			h = 0.022 * safezoneH;
			action = TASK(3);
		};
		class SideMButton5: btc_side_RscButton
		{
			idc = 1604;
			text = "5 - SM - Help Citizens";
			x = 0.298906 * safezoneW + safezoneX;
			y = 0.39 * safezoneH + safezoneY;
			w = 0.150000 * safezoneW;
			h = 0.022 * safezoneH;
			action = TASK(5);
		};
		class SideMButton6: btc_side_RscButton
		{
			idc = 1605;
			text = "6 - SM - Checkpoint";
			x = 0.298906 * safezoneW + safezoneX;
			y = 0.423 * safezoneH + safezoneY;
			w = 0.150000 * safezoneW;
			h = 0.022 * safezoneH;
			action = TASK(6);
		};
		class SideMButton7: btc_side_RscButton
		{
			idc = 1606;
			text = "7 - SM - Help Citizens(Sea)";
			x = 0.298906 * safezoneW + safezoneX;
			y = 0.456 * safezoneH + safezoneY;
			w = 0.150000 * safezoneW;
			h = 0.022 * safezoneH;
			action = TASK(7);
		};
		class SideMButton8: btc_side_RscButton
		{
			idc = 1607;
			text = "8 - SM - Underwater Generator";
			x = 0.298906 * safezoneW + safezoneX;
			y = 0.489 * safezoneH + safezoneY;
			w = 0.150000 * safezoneW;
			h = 0.022 * safezoneH;
			action = TASK(8);
		};
		class SideMButton9: btc_side_RscButton
		{
			idc = 1608;
			text = "9 - SM - Convoy";
			x = 0.298906 * safezoneW + safezoneX;
			y = 0.522 * safezoneH + safezoneY;
			w = 0.150000 * safezoneW;
			h = 0.022 * safezoneH;
			action = TASK(9);
		};
		class SideMButton10: btc_side_RscButton
		{
			idc = 1609;
			text = "10 - SM - Rescue";
			x = 0.298906 * safezoneW + safezoneX;
			y = 0.555 * safezoneH + safezoneY;
			w = 0.150000 * safezoneW;
			h = 0.022 * safezoneH;
			action = TASK(10);
		};
		class SideMButton11: btc_side_RscButton
		{
			idc = 1610;
			text = "11 - SM - Capture Officer";
			x = 0.298906 * safezoneW + safezoneX;
			y = 0.588 * safezoneH + safezoneY;
			w = 0.150000 * safezoneW;
			h = 0.022 * safezoneH;
			action = TASK(11);
		};
		class SideMButton12: btc_side_RscButton
		{
			idc = 1611;
			text = "12 - SM - Hostage";
			x = 0.298906 * safezoneW + safezoneX;
			y = 0.621 * safezoneH + safezoneY;
			w = 0.150000 * safezoneW;
			h = 0.022 * safezoneH;
			action = TASK(12);
		};
		class SideMButton13: btc_side_RscButton
		{
			idc = 1612;
			text = "13 - SM - Hacking";
			x = 0.298906 * safezoneW + safezoneX;
			y = 0.654 * safezoneH + safezoneY;
			w = 0.150000 * safezoneW;
			h = 0.022 * safezoneH;
			action = TASK(13);
		};
		class SideMButton14: btc_side_RscButton
		{
			idc = 1613;
			text = "14 - SM - Tower";
			x = 0.298906 * safezoneW + safezoneX;
			y = 0.687 * safezoneH + safezoneY;
			w = 0.150000 * safezoneW;
			h = 0.022 * safezoneH;
			action = TASK(4);
		};
		class SideMButton15: btc_side_RscButton
		{
			idc = 1614;
			text = "15 - SM - Propaganda";
			x = 0.298906 * safezoneW + safezoneX;
			y = 0.720 * safezoneH + safezoneY;
			w = 0.150000 * safezoneW;
			h = 0.022 * safezoneH;
			action = TASK(14);
		};
	};
};