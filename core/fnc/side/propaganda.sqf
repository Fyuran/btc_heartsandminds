/* ----------------------------------------------------------------------------
Function: btc_fnc_side_propaganda

Description:
    Fill me when you edit me !

Parameters:
    _custom - [String]

Returns:

Examples:
    (begin example)
        _result = [] call btc_fnc_side_propaganda;
    (end)

Author:
    Fyuran

//"Land_Loudspeakers_F"
---------------------------------------------------------------------------- */
params[["_city","DUMMY",[""]]];
#define MAXLOUDSPEAKERS 5

_city = call {
	private _i = btc_city_all findIf {_x getVariable "name" isEqualTo _city};
	if(_i isEqualTo -1) exitWith {selectRandom (btc_city_all select { _x getVariable ["occupied", false] && {!( (_x getVariable ["type", ""]) in ["Hill","RockyArea"])} } ) };
	private _city = btc_city_all select _i;
	_city setVariable ["occupied",true];
	(_city getVariable ["marker", ""]) setMarkerColor "ColorRed";
	_city
};

if (_city isEqualTo []) exitWith {[] spawn btc_fnc_side_create;};

private _radius_x = _city getVariable ["RadiusX", 0];
private _radius_y = _city getVariable ["RadiusY", 0];
private _radius = (_radius_x + _radius_y);
private _pos = getPos _city;
private _loudspeakers = [];
private _m_loudspeakers = [];

btc_side_aborted = false;
btc_side_done = false;
btc_side_failed = false;
btc_side_assigned = true;publicVariable "btc_side_assigned";

[17,_pos,_city getVariable "name"] remoteExec ["btc_fnc_task_create", 0];

btc_side_jip_data = [17,_pos,_city getVariable "name"];

for "_i" from 1 to MAXLOUDSPEAKERS do {
	private _rpos = [_pos, _radius, false] call btc_fnc_randomize_pos;
	_rpos = [_rpos] call btc_fnc_findPosOutsideRock;
	_veh = createVehicle [ "Land_Loudspeakers_F", _rpos, [], 0, "NONE"];
	_veh setPos (_veh modelToWorld [0,0,-5]); //until ACE fixes model action point placement
	_veh enableSimulationGlobal false;
	private _m = createmarker [format ["loudspeaker %1",_i],_rpos];
    _m setmarkertype "mil_dot";
    _m setmarkertext format ["Loudspeaker %1", _i];
	_loudspeakers pushBack _veh;
	_m_loudspeakers pushBack _m;
	private _btc_3Dambience =
	[
	    {
	        playSound3D [MISSION_ROOT + "core\sounds\propaganda_putin.ogg", objNull, false, getPosASL _args, 5, 1, 1000];
	    },
	    300,
	    _veh
	] call CBA_fnc_addPerFrameHandler;
	_veh setVariable ["3dsound", _btc_3Dambience];

    [[_veh,_city],{
    	params["_veh", "_city"];
	    private _action = ["Disable","Disable","\A3\ui_f\data\igui\cfg\simpleTasks\types\intel_ca.paa",{
	    	private _city = _this select 2;
	        [_target,0,["ACE_MainActions","Disable"]] remoteExecCall ["ace_interact_menu_fnc_removeActionFromObject",-2,_target];
	        playSound3D [MISSION_ROOT + "core\sounds\propaganda_micfeedback.ogg", objNull, false, getPos _target, 5, 1, 1000];
	        private _side_loudspeakers = _city getVariable ["disabled_loudspeakers",0];
	        _city setVariable ["disabled_loudspeakers", _side_loudspeakers + 1, true];
	    },{true}, {}, _city] call ace_interact_menu_fnc_createAction;
	    [_veh, 0, ["ACE_MainActions"], _action] call ace_interact_menu_fnc_addActionToObject;
	}] remoteExec ["call", -2, _veh];
};

waitUntil {sleep 5;
	(btc_side_aborted || btc_side_failed || btc_side_done ||
	(_city getVariable ["disabled_loudspeakers",0] isEqualTo MAXLOUDSPEAKERS))
};
if (btc_side_aborted || btc_side_failed) exitWith {
    17 remoteExec ["btc_fnc_task_fail", 0];
    [_m_loudspeakers, _loudspeakers, [], []] call btc_fnc_delete;
    _loudspeakers apply {[_x getVariable "3dsound"] call CBA_fnc_removePerFrameHandler};
    _city setVariable ["disabled_loudspeakers",nil,true];
    btc_city_dummy setPosASL [0,0,0];
    btc_side_assigned = false;publicVariable "btc_side_assigned";
};

private _groups = [];
private _closest = [_city,btc_city_all select {!(_x getVariable ["active",false])},false] call btc_fnc_find_closecity;
for "_i" from 1 to (2 + round random 1) do {
    _groups pushBack ([_closest, getpos _city,1,selectRandom btc_type_motorized] call btc_fnc_mil_send);
};

_groups apply {_x setBehaviour "CARELESS"};
btc_side_assigned = false;publicVariable "btc_side_assigned";

80 call btc_fnc_rep_change;

[_m_loudspeakers, _loudspeakers, [], []] call btc_fnc_delete;
_city setVariable ["disabled_loudspeakers",nil,true];
_loudspeakers apply {[_x getVariable "3dsound"] call CBA_fnc_removePerFrameHandler};

17 remoteExec ["btc_fnc_task_set_done", 0];