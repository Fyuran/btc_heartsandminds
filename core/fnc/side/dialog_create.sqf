/* ----------------------------------------------------------------------------
Function: btc_fnc_side_dialog_create

Description:
    Fill me when you edit me !

Parameters:

Returns:

Examples:
    (begin example)
        _result = [] call btc_fnc_side_dialog_create;
    (end)

Author:
    Fyuran

---------------------------------------------------------------------------- */
params[
    ["_id",0,[0]],
    ["_city","",[""]]
];

if (btc_side_assigned) exitWith {};

btc_side_aborted = false;
btc_side_done = false;
btc_side_failed = false;

switch (_id) do {
    case 0 : {[_city] call btc_fnc_side_supply};
    case 1 : {[_city] call btc_fnc_side_mines};
    case 2 : {[_city] call btc_fnc_side_vehicle};
    case 3 : {[_city] call btc_fnc_side_get_city};
    case 4 : {[_city] call btc_fnc_side_tower};
    case 5 : {[_city] call btc_fnc_side_civtreatment};
    case 6 : {[_city] call btc_fnc_side_checkpoint};
    case 7 : {[_city] call btc_fnc_side_civtreatment_boat};
    case 8 : {[_city] call btc_fnc_side_underwater_generator};
    case 9 : {[_city] call btc_fnc_side_convoy};
    case 10 : {[_city] call btc_fnc_side_rescue};
    case 11 : {[_city] call btc_fnc_side_capture_officer};
    case 12 : {[_city] call btc_fnc_side_hostage};
    case 13 : {[_city] call btc_fnc_side_hack};
    case 14 : {[_city] call btc_fnc_side_propaganda};
    case 15 : {[_city] call btc_fnc_side_bombdefusal};
};