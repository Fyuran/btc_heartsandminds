/* ----------------------------------------------------------------------------
Function: btc_fnc_side_bombdefusal_timer

Description:
    Fill me when you edit me !

Parameters:


Returns:

Examples:
    (begin example)
        _result = [] call btc_fnc_side_bombdefusal_timer;
    (end)

Author:
    Fyuran

---------------------------------------------------------------------------- */
params[
	["_bomb",objNull,[objNull]]
];
if(_bomb getVariable ["bombdefusal_timer_on",false]) exitWith{};

_end_time = ((_bomb getVariable ["bombdefusal_data",[]]) select 1) select 2;
private _start_time = CBA_missionTime;

private _handle = [{
    _args params["_start_time","_end_time","_bomb"];
    _end_time = _bomb getVariable ["bombdefusal_endtime",_end_time];
    _elapsed_time = CBA_missionTime - _start_time;
    _countdown = _end_time - _elapsed_time;
    if(_countdown <= 0) exitWith {[_handle] call CBA_fnc_removePerFrameHandler; btc_side_failed = true};
    _bomb setVariable ["bombdefusal_time",_countdown,true];
    if (btc_debug_log) then {
        [format ["tick-tock: %1", _countdown], __FILE__, [false]] call btc_fnc_debug_message;
    };
}, 1, [_start_time,_end_time,_bomb]] call CBA_fnc_addPerFrameHandler;

_bomb setVariable ["bombdefusal_timer_on",true];
_bomb setVariable ["bombdefusal_timer_handle",_handle];
