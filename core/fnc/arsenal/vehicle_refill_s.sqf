
/* ----------------------------------------------------------------------------
Function: btc_fnc_arsenal_vehicle_refill_s

Description:
    Fill me when you edit me !

Parameters:
    _obj - [Object]
    _radius - [Number]

Returns:

Examples:
    (begin example)
        _result = [] call btc_fnc_arsenal_vehicle_refill_s;
    (end)

Author:
    Fyuran

---------------------------------------------------------------------------- */
params[
    ["_repairdepot",objNull,[objNull]],
    ["_radius",5,[0]]
];

private _trigger = createTrigger["EmptyDetector",_repairdepot];
_trigger setTriggerArea[_radius,_radius,0,false];
_trigger setTriggerActivation["ANYPLAYER","PRESENT",true];

private _trgact = "
    _vehs = nearestObjects [getPosASL thisTrigger, ['LandVehicle'], 300, true];
    _vehs apply {_x setVariable ['refill_trigger',thisTrigger,-2]};
    thisTrigger setVariable ['refill_vehs',_vehs];
    [_vehs] remoteExecCall['btc_fnc_arsenal_vehicle_refill',-2];
    ['btc_sound_refill_on']remoteExecCall['playSound',-2];
";

private _trgdeact = "
    _vehs = thisTrigger getVariable ['refill_vehs',[]];
    _vehs apply {_x setVariable ['refill_trigger',nil,-2]};
    [_vehs] remoteExecCall['btc_fnc_arsenal_vehicle_refill_removeAction',-2];
    ['btc_sound_refill_off']remoteExecCall['playSound',-2];
";

_trigger setTriggerStatements ["this", _trgact, _trgdeact];

if (btc_debug_log) then {
    ["refill action has been added to nearby vehicles", __FILE__, [false]] call btc_fnc_debug_message;
};