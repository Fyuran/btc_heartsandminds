
/* ----------------------------------------------------------------------------
Function: btc_fnc_loadout_prefabs

Description:
    Fill me when you edit me !

Parameters:
    _object - [Object]
    _custom - [String]

Returns:

Examples:
    (begin example)
    [_crate, ["RHS-MG",
		[
			 ["rhs_weap_m249_pip_S","rhsusf_acc_SF3P556","rhsusf_acc_anpeq15side_bk","rhsusf_acc_elcan_3d",["rhsusf_200Rnd_556x45_box",200],[],""],
			 [],[],
			 ["rhs_uniform_cu_ucp",[["ACE_fieldDressing",14],["ACE_EarPlugs",1],["ACE_epinephrine",2],["ACE_Flashlight_XL50",1],["ACE_morphine",10],["SmokeShellGreen",2,1],["SmokeShellRed",1,1]]],
			 ["rhsusf_spcs_ucp_saw",[["rhsusf_200Rnd_556x45_box",3,200]]],
			 ["rhsusf_assault_eagleaiii_ucp",[["rhsusf_200Rnd_556x45_box",4,200]]],
			 "rhsusf_ach_helmet_ESS_ucp",
			 "",
			 [],
			 ["ItemMap","ItemGPS","","ItemCompass","ItemWatch","ACE_NVG_Gen4"]
		 ]
	 ]] call btc_fnc_loadout_prefabs;
    (end)

Author:
    Fyuran

---------------------------------------------------------------------------- */
params [["_object", [], [objNull]], ["_custom", []]];

if(!hasInterface) exitWith {};
_mg =
["RHS-MG",
	[
		 ["rhs_weap_m249_pip_S","rhsusf_acc_SF3P556","rhsusf_acc_anpeq15side_bk","rhsusf_acc_elcan_3d",["rhsusf_200Rnd_556x45_box",200],[],""],
		 [],
		 [],
		 ["rhs_uniform_g3_tan",[["ACE_key_west",1],["ACE_fieldDressing",14],["ACE_EarPlugs",1],["ACE_epinephrine",2],["ACE_Flashlight_XL50",1],["ACE_morphine",10]]],
		 ["rhsusf_mbav_mg",[["SmokeShellGreen",2,1],["SmokeShellRed",1,1]]],
		 ["rhsusf_assault_eagleaiii_coy",[["rhsusf_200Rnd_556x45_box",2,200]]],
		 "rhsusf_mich_helmet_marpatd_norotos_arc_headset",
		 "",
		 [],
		 ["ItemMap","ItemGPS","","ItemCompass","ItemWatch","ACE_NVG_Gen4"]
	 ]
];

_rifleman =
["RHS-RIFLEMAN",
	[
		["rhs_weap_vhsk2","","rhsusf_acc_anpeq15side_bk","rhsusf_acc_su230a",["rhsgref_30rnd_556x45_vhs2",30],[],"rhsusf_acc_grip2"],
		[],
		[],
		["rhs_uniform_g3_tan",[["ACE_key_west",1],["ACE_fieldDressing",14],["ACE_EarPlugs",1],["ACE_epinephrine",2],["ACE_Flashlight_XL50",1],["ACE_morphine",10]]],
		["rhsusf_mbav_rifleman",[["rhsgref_30rnd_556x45_vhs2",8,30],["SmokeShellGreen",2,1],["SmokeShellRed",1,1]]],
		["rhsusf_assault_eagleaiii_coy",[]],
		"rhsusf_mich_helmet_marpatd_norotos_arc_headset",
		"",
		[],
		["ItemMap","ItemGPS","","ItemCompass","ItemWatch","ACE_NVG_Gen4"]
	]
];

_medic =
["RHS-MEDIC",
	[
		["rhs_weap_vhsk2","","rhsusf_acc_anpeq15side_bk","rhsusf_acc_su230a",["rhsgref_30rnd_556x45_vhs2",30],[],"rhsusf_acc_grip2"],
		[],
		[],
		["rhs_uniform_g3_tan",[["ACE_key_west",1],["ACE_fieldDressing",14],["ACE_EarPlugs",1],["ACE_epinephrine",2],["ACE_Flashlight_XL50",1],["ACE_morphine",10]]],
		["rhsusf_mbav_medic",[["rhsgref_30rnd_556x45_vhs2",8,30],["SmokeShellGreen",2,1],["SmokeShellRed",1,1]]],
		["rhsusf_assault_eagleaiii_coy",[]],
		"rhsusf_mich_helmet_marpatd_norotos_arc_headset",
		"",
		[],
		["ItemMap","ItemGPS","","ItemCompass","ItemWatch","ACE_NVG_Gen4"]
	]
];


_loadout = if (_custom isEqualTo []) then {[_mg, _rifleman, _medic]} else {[_custom]};

//object addAction [title, script, arguments, priority, showWindow, hideOnUse, shortcut, condition, radius, unconscious, selection, memoryPoint]

_loadout apply {
	_type = _x select 0;
	_unitloadout = _x select 1;
	_object addAction
	[
		_type,
		{
			params ["_target", "_caller", "_actionId", "_arguments"];
			_caller setUnitLoadout (_this select 3);
		},
		_unitloadout,
		4,
		false,
		true,
		"",
		"true",
		5
	];
};


