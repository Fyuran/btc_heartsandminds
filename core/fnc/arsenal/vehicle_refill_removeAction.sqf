/* ----------------------------------------------------------------------------
Function: btc_fnc_arsenal_vehicle_refill_removeAction

Description:
    Fill me when you edit me !

Parameters:
    _custom - [String]

Returns:

Examples:
    (begin example)
        _result = [] call btc_fnc_arsenal_vehicle_refill_removeAction;
    (end)

Author:
    Fyuran

---------------------------------------------------------------------------- */
params[
	["_vehs",[],[[objNull]]]
];
if(_vehs isEqualTo []) exitWith {};

systemChat "Vehicles refill turned off";

if (btc_debug_log) then {
    [format["refill removed from %1", _vehs], __FILE__, [false]] call btc_fnc_debug_message;
};

_vehs apply {
	_veh = _x;
	_action_ids = _veh getVariable ["refill_actionids", [0,0]];
	_action_ids apply {_veh removeAction _x};
};