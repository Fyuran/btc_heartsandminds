/* ----------------------------------------------------------------------------
Function: btc_fnc_arsenal_vehicle_refill

Description:
    Fill me when you edit me !

Parameters:
    _custom - [String]

Returns:

Examples:
    (begin example)
        _result = [] call btc_fnc_arsenal_vehicle_refill;
    (end)

Author:
    Fyuran

---------------------------------------------------------------------------- */
params[
    ["_list",[],[[objNull]]]
];

if(_list isEqualTo []) exitWith {};

if (btc_debug_log) then {
    [format["refill called with: %1", _list], __FILE__, [false]] call btc_fnc_debug_message;
};

systemChat "Vehicles refill turned on";

_list apply {
	_trigger = _x getVariable ["refill_trigger", objNull];
	if(!isNull _trigger) then {
	     //object addAction [title, script, arguments, priority, showWindow, hideOnUse, shortcut, condition, radius, unconscious, selection, memoryPoint]
	    private _action_refill = _x addAction
	    [
	        format["<t color='%2'>%1</t>",
	        "Clone your ammunition into vehicle cargo.", // %1
	        "#32FF00"], //%2 green
	        {
	            params ["_target"];
	            private _mags = ((magazines player) arrayIntersect (magazines player)); //lazy way to get unique magazine classes
	            _mags apply {
	                _mag = _x;
	                _n = {_x isEqualTo _mag}count(magazines player);
	                _target addMagazineCargoGlobal [_mag, _n];
	            };
	            hintSilent "Ammunition Cloned";
	        },
	        nil,
	        4,
	        false,
	        false,
	        "",
	        "true",
	        5
	    ];

	    private _action_medical = _x addAction
	    [
	        format["<t color='%2'>%1</t>",
	        "Clone your medical(Basic) supplies.", // %1
	        "#008B8B"],//%2 dark cyan
	        {
	            params ["_target"];
	            private _basic_classes = ["ACE_fieldDressing","ACE_morphine","ACE_epinephrine","ACE_bloodIV_500","ACE_bloodIV_250","ACE_bloodIV","ACE_surgicalKit"];
	            _basic_classes apply {
	            	_class = _x;
	            	_n = {_x isEqualTo _class} count ((vestItems player) + (uniformItems player) + (backpackItems player));
	            	_target addItemCargoGlobal [_class, _n];
	            };
	            hintSilent "Medical(Basic) supplies cloned";
	        },
	        nil,
	        3.9,
	        false,
	        false,
	        "",
	        "true",
	        5
	    ];
	    //object addAction [title, script, arguments, priority, showWindow, hideOnUse, shortcut, condition, radius, unconscious, selection, memoryPoint]
	    private _action_clear = _x addAction
	    [
	        format["<t color='%2'>%1</t>",
	        "Clear vehicle cargo.", // %1
	        "#FF0000"],//%2 red
	        {
	            params ["_target"];
	            clearWeaponCargoGlobal _target;
	            clearItemCargoGlobal _target;
	            clearMagazineCargoGlobal _target;
	            hintSilent "Vehicle inventory cleared";
	        },
	        nil,
	        3.8,
	        false,
	        false,
	        "",
	        "true",
	        5
	    ];
	   _x setVariable ["refill_actionids", [_action_refill,_action_medical,_action_clear]];
	};
};