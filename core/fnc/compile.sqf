/////////////////////SERVER\\\\\\\\\\\\\\\\\\\\\
if (isServer) then {
    //ARSENAL
    btc_fnc_arsenal_vehicle_refill_s = compileFinal preprocessFileLineNumbers "core\fnc\arsenal\vehicle_refill_s.sqf";

    //CACHE
    btc_fnc_cache_find_pos = compileFinal preprocessFileLineNumbers "core\fnc\cache\find_pos.sqf";
    btc_fnc_cache_hd_cache = compileFinal preprocessFileLineNumbers "core\fnc\cache\hd_cache.sqf";
    btc_fnc_cache_spawn = compileFinal preprocessFileLineNumbers "core\fnc\cache\spawn.sqf";
    btc_fnc_cache_create = compileFinal preprocessFileLineNumbers "core\fnc\cache\create.sqf";
    btc_fnc_cache_create_attachto = compileFinal preprocessFileLineNumbers "core\fnc\cache\create_attachto.sqf";

    //COMMON
    btc_fnc_check_los = compileFinal preprocessFileLineNumbers "core\fnc\common\check_los.sqf";
    btc_fnc_clean_up = compileFinal preprocessFileLineNumbers "core\fnc\common\clean_up.sqf";
    btc_fnc_create_composition = compileFinal preprocessFileLineNumbers "core\fnc\common\create_composition.sqf";
    btc_fnc_house_addWP = compileFinal preprocessFileLineNumbers "core\fnc\common\house_addWP.sqf";
    btc_fnc_set_damage = compileFinal preprocessFileLineNumbers "core\fnc\common\set_damage.sqf";
    btc_fnc_road_direction = compileFinal preprocessFileLineNumbers "core\fnc\common\road_direction.sqf";
    btc_fnc_findsafepos = compileFinal preprocessFileLineNumbers "core\fnc\common\findsafepos.sqf";
    btc_fnc_set_groupowner = compileFinal preprocessFileLineNumbers "core\fnc\common\set_groupowner.sqf";
    btc_fnc_find_closecity = compileFinal preprocessFileLineNumbers "core\fnc\common\find_closecity.sqf";
    btc_fnc_delete = compileFinal preprocessFileLineNumbers "core\fnc\common\delete.sqf";
    btc_fnc_deleteEntities = compileFinal preprocessFileLineNumbers "core\fnc\common\deleteEntities.sqf";
    btc_fnc_final_phase = compileFinal preprocessFileLineNumbers "core\fnc\common\final_phase.sqf";
    btc_fnc_findPosOutsideRock = compileFinal preprocessFileLineNumbers "core\fnc\common\findposoutsiderock.sqf";
    btc_fnc_set_groupsowner = compileFinal preprocessFileLineNumbers "core\fnc\common\set_groupsowner.sqf";

    //CITY
    btc_fnc_city_activate = compileFinal preprocessFileLineNumbers "core\fnc\city\activate.sqf";
    btc_fnc_city_create = compileFinal preprocessFileLineNumbers "core\fnc\city\create.sqf";
    btc_fnc_city_de_activate = compileFinal preprocessFileLineNumbers "core\fnc\city\de_activate.sqf";
    btc_fnc_city_set_clear = compileFinal preprocessFileLineNumbers "core\fnc\city\set_clear.sqf";
    btc_fnc_city_trigger_player_side = compileFinal preprocessFileLineNumbers "core\fnc\city\trigger_player_side.sqf";
    btc_fnc_city_findPos = compileFinal preprocessFileLineNumbers "core\fnc\city\findPos.sqf";

    //CIV
    btc_fnc_civ_add_weapons = compileFinal preprocessFileLineNumbers "core\fnc\civ\add_weapons.sqf";
    btc_fnc_civ_add_grenade = compileFinal preprocessFileLineNumbers "core\fnc\civ\add_grenade.sqf";
    btc_fnc_civ_get_weapons = compileFinal preprocessFileLineNumbers "core\fnc\civ\get_weapons.sqf";
    btc_fnc_civ_get_grenade = compileFinal preprocessFileLineNumbers "core\fnc\civ\get_grenade.sqf";
    btc_fnc_civ_populate = compileFinal preprocessFileLineNumbers "core\fnc\civ\populate.sqf";
    btc_fnc_civ_create_patrol = compileFinal preprocessFileLineNumbers "core\fnc\civ\create_patrol.sqf";
    btc_fnc_civ_air_traffic = compileFinal preprocessFileLineNumbers "core\fnc\civ\air_traffic.sqf";
    btc_fnc_civ_unit_create = compileFinal preprocessFileLineNumbers "core\fnc\civ\unit_create.sqf";
    btc_fnc_civ_CuratorCivPlaced_s = compileFinal preprocessFileLineNumbers "core\fnc\civ\CuratorCivPlaced_s.sqf";
    btc_fnc_civ_evacuate = compileFinal preprocessFileLineNumbers "core\fnc\civ\evacuate.sqf";

    //DATA
    btc_fnc_data_add_group = compileFinal preprocessFileLineNumbers "core\fnc\data\add_group.sqf";
    btc_fnc_data_get_group = compileFinal preprocessFileLineNumbers "core\fnc\data\get_group.sqf";
    btc_fnc_data_spawn_group = compileFinal preprocessFileLineNumbers "core\fnc\data\spawn_group.sqf";

    //DB
    btc_fnc_db_save = compileFinal preprocessFileLineNumbers "core\fnc\db\save.sqf";
    btc_fnc_db_delete = compileFinal preprocessFileLineNumbers "core\fnc\db\delete.sqf";
    btc_fnc_db_autosave = compileFinal preprocessFileLineNumbers "core\fnc\db\autosave.sqf";
    btc_fnc_db_loadObjectStatus = compileFinal preprocessFileLineNumbers "core\fnc\db\loadObjectStatus.sqf";
    btc_fnc_db_saveObjectStatus = compileFinal preprocessFileLineNumbers "core\fnc\db\saveObjectStatus.sqf";
    btc_fnc_db_loadCargo = compileFinal preprocessFileLineNumbers "core\fnc\db\loadcargo.sqf";

    //EH
    btc_fnc_eh_veh_add_respawn = compileFinal preprocessFileLineNumbers "core\fnc\eh\veh_add_respawn.sqf";
    btc_fnc_eh_veh_killed = compileFinal preprocessFileLineNumbers "core\fnc\eh\veh_killed.sqf";
    btc_fnc_eh_veh_respawn = compileFinal preprocessFileLineNumbers "core\fnc\eh\veh_respawn.sqf";
    btc_fnc_eh_explosives_defuse = compileFinal preprocessFileLineNumbers "core\fnc\eh\explosives_defuse.sqf";
    btc_fnc_eh_handledisconnect = compileFinal preprocessFileLineNumbers "core\fnc\eh\handledisconnect.sqf";
    btc_fnc_eh_buildingchanged = compileFinal preprocessFileLineNumbers "core\fnc\eh\buildingchanged.sqf";
    btc_fnc_eh_suicider = compileFinal preprocessFileLineNumbers "core\fnc\eh\suicider.sqf";
    btc_fnc_eh_server = compileFinal preprocessFileLineNumbers "core\fnc\eh\server.sqf";

    //IED
    btc_fnc_ied_boom = compileFinal preprocessFileLineNumbers "core\fnc\ied\boom.sqf";
    btc_fnc_ied_check = compileFinal preprocessFileLineNumbers "core\fnc\ied\check.sqf";
    btc_fnc_ied_checkLoop = compileFinal preprocessFileLineNumbers "core\fnc\ied\checkLoop.sqf";
    btc_fnc_ied_create = compileFinal preprocessFileLineNumbers "core\fnc\ied\create.sqf";
    btc_fnc_ied_fired_near = compileFinal preprocessFileLineNumbers "core\fnc\ied\fired_near.sqf";
    btc_fnc_ied_init_area = compileFinal preprocessFileLineNumbers "core\fnc\ied\init_area.sqf";
    btc_fnc_ied_suicider_active = compileFinal preprocessFileLineNumbers "core\fnc\ied\suicider_active.sqf";
    btc_fnc_ied_suicider_activeLoop = compileFinal preprocessFileLineNumbers "core\fnc\ied\suicider_activeLoop.sqf";
    btc_fnc_ied_suicider_create = compileFinal preprocessFileLineNumbers "core\fnc\ied\suicider_create.sqf";
    btc_fnc_ied_suiciderLoop = compileFinal preprocessFileLineNumbers "core\fnc\ied\suiciderLoop.sqf";
    btc_fnc_ied_allahu_akbar = compileFinal preprocessFileLineNumbers "core\fnc\ied\allahu_akbar.sqf";
    btc_fnc_ied_drone_active = compileFinal preprocessFileLineNumbers "core\fnc\ied\drone_active.sqf";
    btc_fnc_ied_drone_create = compileFinal preprocessFileLineNumbers "core\fnc\ied\drone_create.sqf";
    btc_fnc_ied_droneLoop = compileFinal preprocessFileLineNumbers "core\fnc\ied\droneLoop.sqf";
    btc_fnc_ied_drone_fire = compileFinal preprocessFileLineNumbers "core\fnc\ied\drone_fire.sqf";

    //INFO
    btc_fnc_info_cache = compileFinal preprocessFileLineNumbers "core\fnc\info\cache.sqf";
    btc_fnc_info_give_intel = compileFinal preprocessFileLineNumbers "core\fnc\info\give_intel.sqf";
    btc_fnc_info_has_intel = compileFinal preprocessFileLineNumbers "core\fnc\info\has_intel.sqf";
    btc_fnc_info_hideout = compileFinal preprocessFileLineNumbers "core\fnc\info\hideout.sqf";

    //FOB
    btc_fnc_fob_create_s = compileFinal preprocessFileLineNumbers "core\fnc\fob\create_s.sqf";
    btc_fnc_fob_dismantle_s = compileFinal preprocessFileLineNumbers "core\fnc\fob\dismantle_s.sqf";

    //MIL
    btc_fnc_mil_addWP = compileFinal preprocessFileLineNumbers "core\fnc\mil\addWP.sqf";
    btc_fnc_mil_check_cap = compileFinal preprocessFileLineNumbers "core\fnc\mil\check_cap.sqf";
    btc_fnc_mil_create_group = compileFinal preprocessFileLineNumbers "core\fnc\mil\create_group.sqf";
    btc_fnc_mil_hd_hideout = compileFinal preprocessFileLineNumbers "core\fnc\mil\hd_hideout.sqf";
    btc_fnc_mil_create_hideout = compileFinal preprocessFileLineNumbers "core\fnc\mil\create_hideout.sqf";
    btc_fnc_mil_create_hideout_composition = compileFinal preprocessFileLineNumbers "core\fnc\mil\create_hideout_composition.sqf";
    btc_fnc_mil_create_static = compileFinal preprocessFileLineNumbers "core\fnc\mil\create_static.sqf";
    btc_fnc_mil_create_patrol = compileFinal preprocessFileLineNumbers "core\fnc\mil\create_patrol.sqf";
    btc_fnc_mil_send = compileFinal preprocessFileLineNumbers "core\fnc\mil\send.sqf";
    btc_fnc_mil_set_skill = compileFinal preprocessFileLineNumbers "core\fnc\mil\set_skill.sqf";
    btc_fnc_mil_unit_create = compileFinal preprocessFileLineNumbers "core\fnc\mil\unit_create.sqf";
    btc_fnc_mil_CuratorMilPlaced_s = compileFinal preprocessFileLineNumbers "core\fnc\mil\CuratorMilPlaced_s.sqf";
    btc_fnc_mil_getStructures = compileFinal preprocessFileLineNumbers "core\fnc\mil\getStructures.sqf";
    btc_fnc_mil_getBuilding = compileFinal preprocessFileLineNumbers "core\fnc\mil\getBuilding.sqf";

    btc_fnc_mil_createVehicle = compileFinal preprocessFileLineNumbers "core\fnc\mil\createVehicle.sqf";
    btc_fnc_mil_createUnits = compileFinal preprocessFileLineNumbers "core\fnc\mil\createUnits.sqf";

    //PATROL
    btc_fnc_patrol_playersInAreaCityGroup = compileFinal preprocessFileLineNumbers "core\fnc\patrol\playersInAreaCityGroup.sqf";
    btc_fnc_patrol_eh = compileFinal preprocessFileLineNumbers "core\fnc\patrol\eh.sqf";
    btc_fnc_patrol_eh_remove = compileFinal preprocessFileLineNumbers "core\fnc\patrol\eh_remove.sqf";
    btc_fnc_patrol_usefulCity = compileFinal preprocessFileLineNumbers "core\fnc\patrol\usefulCity.sqf";
    btc_fnc_patrol_WPCheck = compileFinal preprocessFileLineNumbers "core\fnc\patrol\WPCheck.sqf";
    btc_fnc_patrol_init = compileFinal preprocessFileLineNumbers "core\fnc\patrol\init.sqf";
    btc_fnc_patrol_addWP = compileFinal preprocessFileLineNumbers "core\fnc\patrol\addWP.sqf";

    //REP
    btc_fnc_rep_add_eh = compileFinal preprocessFileLineNumbers "core\fnc\rep\add_eh.sqf";
    btc_fnc_rep_call_militia = compileFinal preprocessFileLineNumbers "core\fnc\rep\call_militia.sqf";
    btc_fnc_rep_change = compileFinal preprocessFileLineNumbers "core\fnc\rep\change.sqf";
    btc_fnc_rep_eh_effects = compileFinal preprocessFileLineNumbers "core\fnc\rep\eh_effects.sqf";
    btc_fnc_rep_hd = compileFinal preprocessFileLineNumbers "core\fnc\rep\hd.sqf";
    btc_fnc_rep_hh = compileFinal preprocessFileLineNumbers "core\fnc\rep\hh.sqf";
    btc_fnc_rep_killed = compileFinal preprocessFileLineNumbers "core\fnc\rep\killed.sqf";
    btc_fnc_rep_firednear = compileFinal preprocessFileLineNumbers "core\fnc\rep\firednear.sqf";
    btc_fnc_rep_remove_eh = compileFinal preprocessFileLineNumbers "core\fnc\rep\remove_eh.sqf";

    //SIDE
    btc_fnc_side_create = compileFinal preprocessFileLineNumbers "core\fnc\side\create.sqf";
    btc_fnc_side_get_city = compileFinal preprocessFileLineNumbers "core\fnc\side\get_city.sqf";
    btc_fnc_side_mines = compileFinal preprocessFileLineNumbers "core\fnc\side\mines.sqf";
    btc_fnc_side_supply = compileFinal preprocessFileLineNumbers "core\fnc\side\supply.sqf";
    btc_fnc_side_vehicle = compileFinal preprocessFileLineNumbers "core\fnc\side\vehicle.sqf";
    btc_fnc_side_civtreatment = compileFinal preprocessFileLineNumbers "core\fnc\side\civtreatment.sqf";
    btc_fnc_side_tower = compileFinal preprocessFileLineNumbers "core\fnc\side\tower.sqf";
    btc_fnc_side_checkpoint = compileFinal preprocessFileLineNumbers "core\fnc\side\checkpoint.sqf";
    btc_fnc_side_civtreatment_boat = compileFinal preprocessFileLineNumbers "core\fnc\side\civtreatment_boat.sqf";
    btc_fnc_side_underwater_generator= compileFinal preprocessFileLineNumbers "core\fnc\side\underwater_generator.sqf";
    btc_fnc_side_convoy = compileFinal preprocessFileLineNumbers "core\fnc\side\convoy.sqf";
    btc_fnc_side_rescue = compileFinal preprocessFileLineNumbers "core\fnc\side\rescue.sqf";
    btc_fnc_side_capture_officer = compileFinal preprocessFileLineNumbers "core\fnc\side\capture_officer.sqf";
    btc_fnc_side_hostage = compileFinal preprocessFileLineNumbers "core\fnc\side\hostage.sqf";
    btc_fnc_side_hack = compileFinal preprocessFileLineNumbers "core\fnc\side\hack.sqf";
    btc_fnc_side_propaganda = compileFinal preprocessFileLineNumbers "core\fnc\side\propaganda.sqf";
    btc_fnc_side_bombdefusal = compileFinal preprocessFileLineNumbers "core\fnc\side\bombdefusal.sqf";
        //BOMB DEFUSAL
        btc_fnc_side_bombdefusal_timer = compileFinal preprocessFileLineNumbers "core\fnc\side\bombdefusal\timer.sqf";
        btc_fnc_side_bombdefusal_test = compileFinal preprocessFileLineNumbers "core\fnc\side\bombdefusal\bombdefusal_test.sqf"; //for purposes of training
        btc_fnc_side_bombdefusal_nuke_fail = compileFinal preprocessFileLineNumbers "core\fnc\side\bombdefusal\nuke_fail.sqf";

    //LOG
    btc_fnc_log_CuratorObjectPlaced_s = compileFinal preprocessFileLineNumbers "core\fnc\log\CuratorObjectPlaced_s.sqf";
    btc_fnc_log_createVehicle = compileFinal preprocessFileLineNumbers "core\fnc\log\createVehicle.sqf";
    btc_fnc_log_getRearmMagazines = compileFinal preprocessFileLineNumbers "core\fnc\log\getRearmMagazines.sqf";
    btc_fnc_log_init = compileFinal preprocessFileLineNumbers "core\fnc\log\init.sqf";

    //DEAF
    btc_fnc_deaf_earringing = compileFinal preprocessFileLineNumbers "core\fnc\deaf\earringing.sqf";

    //CITY_EDITOR
    btc_fnc_cED_ask_markers_s = compileFinal preprocessFileLineNumbers "core\fnc\city_editor\ask_markers_s.sqf";
    btc_fnc_cED_ask_vars = compileFinal preprocessFileLineNumbers "core\fnc\city_editor\ask_vars.sqf";
    btc_fnc_cED_activate = compileFinal preprocessFileLineNumbers "core\fnc\city_editor\activate.sqf";
};

/////////////////////SERVER AND HEADLESS\\\\\\\\\\\\\\\\\\\\\
if (isServer OR (!isDedicated && !hasInterface)) then {
    //MIL
    btc_fnc_mil_unit_killed = compileFinal preprocessFileLineNumbers "core\fnc\mil\unit_killed.sqf";
    btc_fnc_mil_add_eh = compileFinal preprocessFileLineNumbers "core\fnc\mil\add_eh.sqf";

    //DEBUG
    btc_fnc_debug_marker_show_fps = compileFinal preprocessFileLineNumbers "core\fnc\debug\marker_show_fps.sqf";
};

/////////////////////CLIENT AND SERVER\\\\\\\\\\\\\\\\\\\\\

//COMMON
btc_fnc_find_veh_with_turret = compileFinal preprocessFileLineNumbers "core\fnc\common\find_veh_with_turret.sqf";
btc_fnc_get_class = compileFinal preprocessFileLineNumbers "core\fnc\common\get_class.sqf";
btc_fnc_randomize_pos = compileFinal preprocessFileLineNumbers "core\fnc\common\randomize_pos.sqf";
btc_fnc_getHouses = compileFinal preprocessFileLineNumbers "core\fnc\common\getHouses.sqf";
btc_fnc_house_addWP_loop = compileFinal preprocessFileLineNumbers "core\fnc\common\house_addWP_loop.sqf";

//DEBUG
btc_fnc_debug_message = compileFinal preprocessFileLineNumbers "core\fnc\debug\message.sqf";

//DB
btc_fnc_db_add_veh = compileFinal preprocessFileLineNumbers "core\fnc\db\add_veh.sqf";

//CIV
btc_fnc_civ_class = compileFinal preprocessFileLineNumbers "core\fnc\civ\class.sqf";
btc_fnc_civ_addWP = compileFinal preprocessFileLineNumbers "core\fnc\civ\addWP.sqf";

//IED
btc_fnc_ied_belt = compileFinal preprocessFileLineNumbers "core\fnc\ied\belt.sqf";

//INT
btc_fnc_int_orders_give = compileFinal preprocessFileLineNumbers "core\fnc\int\orders_give.sqf";
btc_fnc_int_orders_behaviour = compileFinal preprocessFileLineNumbers "core\fnc\int\orders_behaviour.sqf";
btc_fnc_int_ask_var = compileFinal preprocessFileLineNumbers "core\fnc\int\ask_var.sqf";

//LOG
btc_fnc_log_can_tow = compileFinal preprocessFileLineNumbers "core\fnc\log\can_tow.sqf";
btc_fnc_log_create = compileFinal preprocessFileLineNumbers "core\fnc\log\create.sqf";
btc_fnc_log_create_apply = compileFinal preprocessFileLineNumbers "core\fnc\log\create_apply.sqf";
btc_fnc_log_create_load = compileFinal preprocessFileLineNumbers "core\fnc\log\create_load.sqf";
btc_fnc_log_create_change_target = compileFinal preprocessFileLineNumbers "core\fnc\log\create_change_target.sqf";
btc_fnc_log_create_s = compileFinal preprocessFileLineNumbers "core\fnc\log\create_s.sqf";
btc_fnc_log_hook = compileFinal preprocessFileLineNumbers "core\fnc\log\hook.sqf";
btc_fnc_log_lift_check = compileFinal preprocessFileLineNumbers "core\fnc\log\lift_check.sqf";
btc_fnc_log_lift_deploy_ropes = compileFinal preprocessFileLineNumbers "core\fnc\log\lift_deploy_ropes.sqf";
btc_fnc_log_lift_destroy_ropes = compileFinal preprocessFileLineNumbers "core\fnc\log\lift_destroy_ropes.sqf";
btc_fnc_log_lift_hook = compileFinal preprocessFileLineNumbers "core\fnc\log\lift_hook.sqf";
btc_fnc_log_lift_hook_fake = compileFinal preprocessFileLineNumbers "core\fnc\log\lift_hook_fake.sqf";
btc_fnc_log_lift_hud = compileFinal preprocessFileLineNumbers "core\fnc\log\lift_hud.sqf";
btc_fnc_log_lift_hud_loop = compileFinal preprocessFileLineNumbers "core\fnc\log\lift_hud_loop.sqf";
btc_fnc_log_get_cc = compileFinal preprocessFileLineNumbers "core\fnc\log\get_cc.sqf";
btc_fnc_log_get_rc = compileFinal preprocessFileLineNumbers "core\fnc\log\get_rc.sqf";
btc_fnc_log_place = compileFinal preprocessFileLineNumbers "core\fnc\log\place.sqf";
btc_fnc_log_place_create_camera = compileFinal preprocessFileLineNumbers "core\fnc\log\place_create_camera.sqf";
btc_fnc_log_place_destroy_camera = compileFinal preprocessFileLineNumbers "core\fnc\log\place_destroy_camera.sqf";
btc_fnc_log_place_key_down = compileFinal preprocessFileLineNumbers "core\fnc\log\place_key_down.sqf";
btc_fnc_log_repair_wreck = compileFinal preprocessFileLineNumbers "core\fnc\log\repair_wreck.sqf";
btc_fnc_log_server_repair_wreck = compileFinal preprocessFileLineNumbers "core\fnc\log\server_repair_wreck.sqf";
btc_fnc_log_tow = compileFinal preprocessFileLineNumbers "core\fnc\log\tow.sqf";
btc_fnc_log_unhook = compileFinal preprocessFileLineNumbers "core\fnc\log\unhook.sqf";
btc_fnc_log_copy = compileFinal preprocessFileLineNumbers "core\fnc\log\copy.sqf";
btc_fnc_log_paste = compileFinal preprocessFileLineNumbers "core\fnc\log\paste.sqf";

//MIL
btc_fnc_mil_class = compileFinal preprocessFileLineNumbers "core\fnc\mil\class.sqf";
btc_fnc_mil_ammoUsage = compileFinal preprocessFileLineNumbers "core\fnc\mil\ammoUsage.sqf";

//ARSENAL
btc_fnc_arsenal_ammoUsage = compileFinal preprocessFileLineNumbers "core\fnc\arsenal\ammoUsage.sqf";

//TASK
btc_fnc_task_create = compileFinal preprocessFileLineNumbers "core\fnc\task\create.sqf";
btc_fnc_task_fail = compileFinal preprocessFileLineNumbers "core\fnc\task\fail.sqf";
btc_fnc_task_set_done = compileFinal preprocessFileLineNumbers "core\fnc\task\set_done.sqf";

//SIDE
btc_fnc_side_abort = compileFinal preprocessFileLineNumbers "core\fnc\side\abort.sqf";

/////////////////////CLIENT\\\\\\\\\\\\\\\\\\\\\
if (!isDedicated) then {
    //DB
    btc_fnc_db_request_save = compileFinal preprocessFileLineNumbers "core\fnc\db\request_save.sqf";
    btc_fnc_db_request_delete = compileFinal preprocessFileLineNumbers "core\fnc\db\request_delete.sqf";

    //COMMON
    btc_fnc_end_mission = compileFinal preprocessFileLineNumbers "core\fnc\common\end_mission.sqf";
    btc_fnc_get_cardinal = compileFinal preprocessFileLineNumbers "core\fnc\common\get_cardinal.sqf";
    btc_fnc_show_hint = compileFinal preprocessFileLineNumbers "core\fnc\common\show_hint.sqf";
    btc_fnc_intro = compileFinal preprocessFileLineNumbers "core\fnc\common\intro.sqf";
    btc_fnc_set_markerTextLocal = compileFinal preprocessFileLineNumbers "core\fnc\common\set_markerTextLocal.sqf";
    btc_fnc_showSubtitle = compileFinal preprocessFileLineNumbers "core\fnc\common\showSubtitle.sqf";
    btc_fnc_strategicMapOpen = compileFinal preprocessFileLineNumbers "core\fnc\common\strategicMapOpen.sqf";

    //DEBUG
    btc_fnc_debug_marker = compileFinal preprocessFileLineNumbers "core\fnc\debug\marker.sqf";
    btc_fnc_debug_units = compileFinal preprocessFileLineNumbers "core\fnc\debug\units.sqf";
    btc_fnc_debug_fps = compileFinal preprocessFileLineNumbers "core\fnc\debug\fps.sqf";
    btc_fnc_debug_graph = compileFinal preprocessFileLineNumbers "core\fnc\debug\graph.sqf";

    //CIV
    btc_fnc_civ_add_leaflets = compileFinal preprocessFileLineNumbers "core\fnc\civ\add_leaflets.sqf";

    //IED
    btc_fnc_ied_effects = compileFinal preprocessFileLineNumbers "core\fnc\ied\effects.sqf";
    btc_fnc_ied_effect_smoke = compileFinal preprocessFileLineNumbers "core\fnc\ied\effect_smoke.sqf";
    btc_fnc_ied_effect_color_smoke = compileFinal preprocessFileLineNumbers "core\fnc\ied\effect_color_smoke.sqf";
    btc_fnc_ied_effect_rocks = compileFinal preprocessFileLineNumbers "core\fnc\ied\effect_rocks.sqf";
    btc_fnc_ied_effect_blurEffect = compileFinal preprocessFileLineNumbers "core\fnc\ied\effect_blurEffect.sqf";
    btc_fnc_ied_effect_shock_wave = compileFinal preprocessFileLineNumbers "core\fnc\ied\effect_shock_wave.sqf";

    //EH
    btc_fnc_eh_player_respawn = compileFinal preprocessFileLineNumbers "core\fnc\eh\player_respawn.sqf";
    btc_fnc_eh_CuratorObjectPlaced = compileFinal preprocessFileLineNumbers "core\fnc\eh\CuratorObjectPlaced.sqf";
    btc_fnc_eh_treatment = compileFinal preprocessFileLineNumbers "core\fnc\eh\treatment.sqf";
    btc_fnc_eh_leaflets = compileFinal preprocessFileLineNumbers "core\fnc\eh\leaflets.sqf";
    btc_fnc_eh_player = compileFinal preprocessFileLineNumbers "core\fnc\eh\player.sqf";

    //FOB
    btc_fnc_fob_create = compileFinal preprocessFileLineNumbers "core\fnc\fob\create.sqf";
    btc_fnc_fob_redeploy = compileFinal preprocessFileLineNumbers "core\fnc\fob\redeploy.sqf";
    btc_fnc_fob_dismantle = compileFinal preprocessFileLineNumbers "core\fnc\fob\dismantle.sqf";

    //INT
    btc_fnc_int_add_actions = compileFinal preprocessFileLineNumbers "core\fnc\int\add_actions.sqf";
    btc_fnc_int_action_result = compileFinal preprocessFileLineNumbers "core\fnc\int\action_result.sqf";
    btc_fnc_int_orders = compileFinal preprocessFileLineNumbers "core\fnc\int\orders.sqf";
    btc_fnc_int_shortcuts = compileFinal preprocessFileLineNumbers "core\fnc\int\shortcuts.sqf";
    btc_fnc_int_terminal = compileFinal preprocessFileLineNumbers "core\fnc\int\terminal.sqf";

    //INFO
    btc_fnc_info_ask = compileFinal preprocessFileLineNumbers "core\fnc\info\ask.sqf";
    btc_fnc_info_hideout_asked = compileFinal preprocessFileLineNumbers "core\fnc\info\hideout_asked.sqf";
    btc_fnc_info_search_for_intel = compileFinal preprocessFileLineNumbers "core\fnc\info\search_for_intel.sqf";
    btc_fnc_info_troops = compileFinal preprocessFileLineNumbers "core\fnc\info\troops.sqf";
    btc_fnc_info_ask_reputation = compileFinal preprocessFileLineNumbers "core\fnc\info\ask_reputation.sqf";

    //LOG
    btc_fnc_log_hitch_points = compileFinal preprocessFileLineNumbers "core\fnc\log\hitch_points.sqf";
    btc_fnc_log_get_corner_points = compileFinal preprocessFileLineNumbers "core\fnc\log\get_corner_points.sqf";

    //ARSENAL
    btc_fnc_arsenal_data = compileFinal preprocessFileLineNumbers "core\fnc\arsenal\data.sqf";
    btc_fnc_arsenal_garage = compileFinal preprocessFileLineNumbers "core\fnc\arsenal\garage.sqf";
    btc_fnc_arsenal_loadout = compileFinal preprocessFileLineNumbers "core\fnc\arsenal\loadout.sqf";
    btc_fnc_arsenal_trait = compileFinal preprocessFileLineNumbers "core\fnc\arsenal\trait.sqf";
    btc_fnc_arsenal_ammoUsage = compileFinal preprocessFileLineNumbers "core\fnc\arsenal\ammoUsage.sqf";
    btc_fnc_arsenal_weaponsFilter = compileFinal preprocessFileLineNumbers "core\fnc\arsenal\weaponsfilter.sqf";
    btc_fnc_arsenal_loadout_prefabs = compileFinal preprocessFileLineNumbers "core\fnc\arsenal\loadout_prefabs.sqf";
    btc_fnc_arsenal_vehicle_refill = compileFinal preprocessFileLineNumbers "core\fnc\arsenal\vehicle_refill.sqf";
    btc_fnc_arsenal_vehicle_refill_removeAction = compileFinal preprocessFileLineNumbers "core\fnc\arsenal\vehicle_refill_removeAction.sqf";

    //TASK
    btc_fnc_task_create = compileFinal preprocessFileLineNumbers "core\fnc\task\create.sqf";
    btc_fnc_task_fail = compileFinal preprocessFileLineNumbers "core\fnc\task\fail.sqf";
    btc_fnc_task_set_done = compileFinal preprocessFileLineNumbers "core\fnc\task\set_done.sqf";

    //SIDE
    btc_fnc_side_request = compileFinal preprocessFileLineNumbers "core\fnc\side\request.sqf";
        //BOMB DEFUSAL
        btc_fnc_side_bombdefusal_gui_digit = compileFinal preprocessFileLineNumbers "core\fnc\side\bombdefusal\gui_digit.sqf";
        btc_fnc_side_bombdefusal_gui_accept = compileFinal preprocessFileLineNumbers "core\fnc\side\bombdefusal\gui_accept.sqf";
        btc_fnc_side_bombdefusal_gui_clear = compileFinal preprocessFileLineNumbers "core\fnc\side\bombdefusal\gui_clear.sqf";
        btc_fnc_side_bombdefusal_gui_timer = compileFinal preprocessFileLineNumbers "core\fnc\side\bombdefusal\gui_timer.sqf";

    /*EFFECTS
    btc_fnc_effects_ash = compileFinal preprocessFileLineNumbers "core\fnc\effects\ash.sqf";
    btc_fnc_effects_isInside = compileFinal preprocessFileLineNumbers "core\fnc\effects\isInside.sqf";
    */
    //CITY_EDITOR
    btc_fnc_cED_ask_markers = compileFinal preprocessFileLineNumbers "core\fnc\city_editor\ask_markers.sqf";
    btc_fnc_cED = compileFinal preprocessFileLineNumbers "core\fnc\city_editor\city_editor.sqf";
    btc_fnc_cED_vars_edit = compileFinal preprocessFileLineNumbers "core\fnc\city_editor\vars_edit.sqf";
    btc_fnc_cED_marker_debug = compileFinal preprocessFileLineNumbers "core\fnc\city_editor\marker_debug.sqf";
};
