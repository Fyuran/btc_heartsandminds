
/* ----------------------------------------------------------------------------
Function: btc_fnc_cED

Description:

Parameters:

Returns:

Examples:
    (begin example)
    (end)

Author:
    =BTC=Fyuran

---------------------------------------------------------------------------- */

if(isNil "btc_city_all") then {[clientOwner,"btc_city_all"]remoteExecCall["publicVariableClient",2]};
[{!isNil "btc_city_all"},{
	if((player getVariable ["btc_debug_asked_markers",[]]) isEqualTo []) then {[player]remoteExecCall["btc_fnc_cED_ask_markers_s",2]};
	hint "Click on city to edit, alt+left click to teleport and exit map to turn off editor";

	private _markers_handle = ((findDisplay 12) displayCtrl 51) ctrlAddEventHandler ["Draw", btc_fnc_cED_marker_debug];
	private _edit_handle = ["btc_city_editor", "onMapSingleClick", {
		if(_alt) exitWith {vehicle player setPos _pos};
		private _city = _pos nearObjects 10;
		private _i = _city findif {_x in btc_city_all};
		hint format ["%1",_city];
		if(_i != -1) then {
			_city = _city select _i;
			[_city,player] remoteExecCall ["btc_fnc_cED_ask_vars",2];
			[{!(player getVariable ["btc_city_editor_data", []] isEqualTo [])}, {createDialog "btc_city_editor_dialog";}, []] call CBA_fnc_waitUntilAndExecute;
		};
	}, []] call BIS_fnc_addStackedEventHandler;

	[{!visibleMap}, {
		params["_edit_handle","_markers_handle"];
		[_edit_handle, "onMapSingleClick"] call BIS_fnc_removeStackedEventHandler;
		((findDisplay 12) displayCtrl 51) ctrlRemoveEventHandler ["Draw",_markers_handle];
		hint "editor turned off";
		player setVariable ["btc_city_editor_data", []];
		[] call btc_fnc_cED_ask_markers;
	}, [_edit_handle,_markers_handle]] call CBA_fnc_waitUntilAndExecute;
},[]]call CBA_fnc_waitUntilAndExecute;