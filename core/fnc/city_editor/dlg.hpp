/*
	_city	0
	_city getVariable "id"	1
	_city getVariable "name"	2
	(_city getVariable "RadiusX") + btc_city_radius	3
	(_city getVariable "RadiusY") + btc_city_radius	4
	_city getVariable "trigger_player_side"	5
	_city getVariable "occupied"	6
	_city getVariable "initialized"	7
*/
class btc_city_editor_dialog {
	idd = 19338;
	MovingEnabled = true;

	class controls {
		class btc_dlg_city_editor_occupied: RscButton
		{
			idc = -1;
			text = "Occupied?";
			x = 0.5825 * safezoneW + safezoneX;
			y = 0.357 * safezoneH + safezoneY;
			w = 0.12375 * safezoneW;
			h = 0.044 * safezoneH;
			action = "\
				_occupied = (player getVariable ['btc_city_editor_data',[]]) select 6;\
				['occupied',!(_occupied),6] call btc_fnc_cED_vars_edit;\
			";
		};
		class btc_dlg_city_editor_activate: RscButton
		{
			idc = -1;
			text = "Activated?";
			x = 0.5825 * safezoneW + safezoneX;
			y = 0.401 * safezoneH + safezoneY;
			w = 0.12375 * safezoneW;
			h = 0.044 * safezoneH;
			action = "\
				_id = (player getVariable ['btc_city_editor_data',[]]) select 1;\
				[_id] remoteExecCall ['btc_fnc_cED_activate',2];\
				[] call btc_fnc_cED_ask_markers;\
				[] call btc_fnc_cED_ask_markers;\
			";
		};
		class btc_dlg_city_editor_title: RscText
		{
			idc = -1;
			text = "City:";
			x = 0.5825 * safezoneW + safezoneX;
			y = 0.335 * safezoneH + safezoneY;
			w = 0.04125 * safezoneW;
			h = 0.022 * safezoneH;
			colorText[] = {1,1,1,1};
			colorBackground[] = {0,0,0,1};
		};
		class btc_dlg_city_editor_title_id: RscText
		{
			idc = -1;
			text = "";
			onLoad = "(_this select 0) ctrlSetText ((player getvariable ['btc_city_editor_data','']) select 2)";
			x = 0.62375 * safezoneW + safezoneX;
			y = 0.335 * safezoneH + safezoneY;
			w = 0.0825 * safezoneW;
			h = 0.022 * safezoneH;
			colorText[] = {1,1,1,1};
			colorBackground[] = {0,0,0,1};
		};
	};
};