
/* ----------------------------------------------------------------------------
Function: btc_fnc_cED_ask_markers

Description:

Parameters:

Returns:

Examples:
    (begin example)
    (end)

Author:
    =BTC=Fyuran

---------------------------------------------------------------------------- */
private _markers = player getVariable ["btc_debug_asked_markers",[]];
if(_markers isEqualTo []) then {
	[player]remoteExecCall["btc_fnc_cED_ask_markers_s",2];
}else{
	_markers apply {_x apply {deleteMarkerLocal _x}}; //delete nested markers [[marker1,marke1],[marker2,marke2]...]
	(player setVariable ["btc_debug_asked_markers",[]]);
};