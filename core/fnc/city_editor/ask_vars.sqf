
/* ----------------------------------------------------------------------------
Function: btc_fnc_cED_ask_vars

Description:

Parameters:

Returns:

Examples:
    (begin example)
    (end)

Author:
    =BTC=Fyuran

---------------------------------------------------------------------------- */
if(!params[
	["_city",objNull,[objNull]],
	["_client",objNull,[objNull]]
])exitWith{diag_log format["btc_fnc_cED_ask_vars: bad params %1, %2",_city,_client]};

private _i = btc_city_all findIf {_x == _city};
if(_i == -1) exitWith {diag_log format ["btc_fnc_cED_ask_vars: %1 could not be found",_city]};
private _city = btc_city_all select _i;

private _data = [
	_city,
	_city getVariable ["id",0],
	_city getVariable ["name",""],
	(_city getVariable ["RadiusX",100]) + btc_city_radius,
	(_city getVariable ["RadiusY",100]) + btc_city_radius,
	_city getVariable ["trigger_player_side",objNull],
	_city getVariable ["occupied",false]
];

_client setVariable["btc_city_editor_data", _data, groupOwner group _client];