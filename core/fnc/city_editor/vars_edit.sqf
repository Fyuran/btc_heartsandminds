
/* ----------------------------------------------------------------------------
Function: btc_fnc_cED_vars_edit

Description:
	_city	0
	_city getVariable "id"	1
	_city getVariable "name"	2
	(_city getVariable "RadiusX") + btc_city_radius	3
	(_city getVariable "RadiusY") + btc_city_radius	4
	_city getVariable "trigger_player_side"	5
	_city getVariable "occupied"	6
	_city getVariable "initialized"	7
Parameters:

Returns:

Examples:
    (begin example)
    (end)

Author:
    =BTC=Fyuran

---------------------------------------------------------------------------- */
if(!params[
	["_var","", [""]],
	["_value", 0, [0,true]],
	["_slot",0,[0]]
])exitWith{diag_log format["btc_fnc_cED_vars_edit: bad params %1, %2, %3",_var, _value, _slot]};

private _new_city_editor_data = player getVariable ["btc_city_editor_data", []];
private _city = _new_city_editor_data select 0;
_city setVariable [_var,_value,2]; //change server city value
_new_city_editor_data set[_slot,_value]; //then refresh it locally to reflect changes
player setVariable ["btc_city_editor_data", _new_city_editor_data];

//[] call btc_fnc_cED_ask_markers is called twice to delete and then create refreshed markers.
[] call btc_fnc_cED_ask_markers;
[] call btc_fnc_cED_ask_markers;
