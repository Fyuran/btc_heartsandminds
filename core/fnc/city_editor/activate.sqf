
/* ----------------------------------------------------------------------------
Function: btc_fnc_cED_activate

Description:
    Triggers city activation by invisible AI dummy beloging to btc_player_side

Parameters:
    _name - Name of the city. [String]


Returns:

Examples:
    (begin example)
    (end)

Author:
    Fyuran

---------------------------------------------------------------------------- */
params[
	["_id",0,[0]]
];

private _city = btc_city_all select _id;
private _dummy = _city getVariable ["btc_cED_dummy",objNull];
if(!isNull _dummy) exitWith{
	private _group = group _dummy;
	deleteVehicle _dummy;
	deleteGroup _group;
	_city setVariable ["btc_cED_dummy",objNull];
};

private _pos = getPosASL _city;
_pos set[1,(_pos select 1) + 10]; //dummy would overlap with city marker making readability a mess.
private _group = createGroup [btc_player_side, true];
_dummy = _group createUnit ["B_Soldier_VR_F", _pos, [], 0, "CAN_COLLIDE"];
_group setVariable ["no_cache",true];
_dummy setVariable ["acex_headless_blacklist", true];
_dummy enableSimulationGlobal false;
_dummy hideObjectGlobal true;
_city setVariable ["btc_cED_dummy",_dummy];

if (btc_debug_log) then {
    [format ["btc_cED_dummy placed at city %1 - pos %2", _id, _pos], __FILE__, [false]] call btc_fnc_debug_message;
};