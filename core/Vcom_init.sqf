VCM_ARTYENABLE = false;
VCM_MINECHANCE = 10;
VCM_FFEARTILLERY = false;
VCM_RAGDOLL = false;
VCM_HEARINGDISTANCE = 300;
VCM_WARNDIST = 300;
VCM_WARNDELAY = 30;
VCM_StealVeh = true;
VCM_AIDISTANCEVEHPATH = 100;

VCM_AIDIFA = [['aimingAccuracy',0.025],['aimingShake',0.025],['aimingSpeed',0.025],['commanding',1],['courage',1],['endurance',1],['general',1],['reloadSpeed',1],['spotDistance',0.50],['spotTime',0.85]];

VCM_AIDIFWEST = [['aimingAccuracy',0.025],['aimingShake',0.025],['aimingSpeed',0.025],['commanding',1],['courage',1],['endurance',1],['general',1],['reloadSpeed',1],['spotDistance',0.50],['spotTime',0.85]];
VCM_AIDIFEAST = [['aimingAccuracy',0.025],['aimingShake',0.025],['aimingSpeed',0.025],['commanding',1],['courage',1],['endurance',1],['general',1],['reloadSpeed',1],['spotDistance',0.50],['spotTime',0.85]];
VCM_AIDIFRESISTANCE = [['aimingAccuracy',0.025],['aimingShake',0.025],['aimingSpeed',0.025],['commanding',1],['courage',1],['endurance',1],['general',1],['reloadSpeed',1],['spotDistance',0.50],['spotTime',0.85]];