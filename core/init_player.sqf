[] call compile preprocessFileLineNumbers "core\doc.sqf";

[{!isNull player}, {

    player addRating 9999;
    btc_effects_enabled = true;
    #include "Vcom_init.sqf"
    _handle =
    [
        {
            if(goggles player in ["skn_m04_gas_mask_bare_dry","skn_m04_gas_mask_blk","skn_m04_gas_mask_blu","skn_m04_gas_mask_gre",
                "skn_m50_gas_mask","skn_m50_gas_mask_hood","skn_m50_gas_mask_hood_wd","skn_s10_balaclava_blue_dry","skn_s10_balaclava_red_dry","skn_s10_balaclava_white_dry","skn_s10_balaclava_yellow_dry"]) then {playSound "mask_breath"};
        }, 7, []
    ] call CBA_fnc_addPerFrameHandler;
    ["InitializePlayer", [player]] call BIS_fnc_dynamicGroups;
    [player] call btc_fnc_eh_player;
    private _arsenal_trait = player call btc_fnc_arsenal_trait;
    if (btc_p_arsenal_Restrict isEqualTo 3) then {
        [_arsenal_trait select 1] call btc_fnc_arsenal_weaponsFilter;
    };
    [] call btc_fnc_int_add_actions;
    [] call btc_fnc_int_shortcuts;

    if (player getVariable ["interpreter", false]) then {
        player createDiarySubject [localize "STR_BTC_HAM_CON_INFO_ASKHIDEOUT_DIARYLOG", localize "STR_BTC_HAM_CON_INFO_ASKHIDEOUT_DIARYLOG"];
    };

    if (btc_p_autoloadout) then {
        player setUnitLoadout ([_arsenal_trait select 0] call btc_fnc_arsenal_loadout);
    } else {
        removeAllWeapons player;
    };

    [{scriptDone btc_intro_done;}, {
        private _standard_tasks = (player call BIS_fnc_tasksUnit) select {
                    [_x] call BIS_fnc_taskState isEqualTo "ASSIGNED" &&
                    _x in ["0", "1", "2"]
                };
        {
            [_x] call btc_fnc_task_create
        } forEach _standard_tasks;

        btc_int_ask_data = nil;
        ["btc_side_jip_data"] remoteExecCall ["btc_fnc_int_ask_var", 2];

        [{!(isNil "btc_int_ask_data")}, {
            private _side_jip_data = btc_int_ask_data;
            if !(_side_jip_data isEqualTo []) then {
                _side_jip_data call btc_fnc_task_create;
            };
        }] call CBA_fnc_waitUntilAndExecute;
    }] call CBA_fnc_waitUntilAndExecute;
}] call CBA_fnc_waitUntilAndExecute;

if (btc_debug) then {
    player allowDamage false;

    waitUntil {!isNull (findDisplay 12)};
    private _eh = ((findDisplay 12) displayCtrl 51) ctrlAddEventHandler ["Draw", btc_fnc_debug_marker];
};
