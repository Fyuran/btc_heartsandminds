class CfgSounds {
	class btc_sound_bombdefusal_release{
		name = "btc_sound_bombdefusal_release";
		sound[] = {"core\sounds\bombdefusal_release.ogg", 0.5, 1};
		titles[] = {};
	};
	class btc_sound_bombdefusal_rollover{
		name = "btc_sound_bombdefusal_rollover";
		sound[] = {"core\sounds\bombdefusal_rollover.ogg", 0.5, 1};
		titles[] = {};
	};
	class btc_sound_bombdefusal_cancel{
		name = "btc_sound_bombdefusal_cancel";
		sound[] = {"core\sounds\bombdefusal_cancel.ogg", 0.5, 1};
		titles[] = {};
	};
	class btc_sound_bombdefusal_error{
		name = "btc_sound_bombdefusal_cancel";
		sound[] = {"core\sounds\bombdefusal_error.ogg", 0.5, 1};
		titles[] = {};
	};
	class btc_sound_refill_on{
		name = "btc_sound_refill_on";
		sound[] = {"core\sounds\refill_on.ogg", 0.3, 1};
		titles[] = {};
	};
	class btc_sound_refill_off{
		name = "btc_sound_refill_off";
		sound[] = {"core\sounds\refill_off.ogg", 0.3, 1};
		titles[] = {};
	};
	class mask_breath
	{
		name = "mask_breath";
		sound[] = {"core\sounds\mask_breath.ogg", 0.5, 1};
		titles[] = {};
	};
};